import random

import numpy as np
import pytest

from landscapegen.hill_generator import (
    HillGeneratorConfig,
    HillGenerator,
    ConstantHillPhase,
    GenericHillPhase,
)
from landscapegen.utils.abstract import PhasedRandomGenerator


@pytest.fixture(scope="session")
def random_seed():
    return random.SystemRandom().getrandbits(128)


@pytest.fixture
def test_hillgen_config(random_seed: int) -> HillGeneratorConfig:
    return HillGeneratorConfig(
        random=PhasedRandomGenerator.create(random_seed),
        cache=True,
        stage_resolution=(300, 300),
        collect_stage_stack=False,
    )


@pytest.mark.parametrize(
    "seed_a,seed_b,phases_a,phases_b,expected",
    (
        (0, 1, [], [], False),
        (0, 0, [], [], True),
        (1, 1, [], [], True),
        (1, 0, [], [], False),
        (
            0,
            0,
            [ConstantHillPhase((0.5, 0.5), (0.5, 0.5), 10)],
            [ConstantHillPhase((0.5, 0.5), (0.5, 0.5), 11)],
            False,
        ),
        (
            0,
            0,
            [ConstantHillPhase((0.5, 0.5), (0.5, 0.5), 10)],
            [ConstantHillPhase((0.5, 0.5), (0.5, 0.6), 10)],
            False,
        ),
        (
            0,
            0,
            [ConstantHillPhase((0.5, 0.5), (0.5, 0.5), 10)],
            [ConstantHillPhase((0.5, 0.5), (0.5, 0.5), 10)],
            True,
        ),
        (
            0,
            0,
            [
                GenericHillPhase(
                    hill_count=10,
                    mean=((0.5, 1), (0.1, 0.5)),
                    stddev=((0.5, 1), (0.1, 0.5)),
                    height=(10, 11),
                    introduce_skew=True,
                )
            ],
            [
                GenericHillPhase(
                    hill_count=10,
                    mean=((0.5, 1), (0.1, 0.5)),
                    stddev=((0.5, 1), (0.1, 0.5)),
                    height=(10, 11),
                    introduce_skew=True,
                )
            ],
            True,
        ),
        (
            0,
            0,
            [
                GenericHillPhase(
                    hill_count=10,
                    mean=((0.5, 1), (0.1, 0.5)),
                    stddev=((0.5, 1), (0.1, 0.5)),
                    height=(10, 11),
                    introduce_skew=True,
                )
            ],
            [
                GenericHillPhase(
                    hill_count=11,
                    mean=((0.5, 1), (0.1, 0.5)),
                    stddev=((0.5, 1), (0.1, 0.5)),
                    height=(10, 11),
                    introduce_skew=True,
                )
            ],
            False,
        ),
        (
            0,
            0,
            [
                GenericHillPhase(
                    hill_count=10,
                    mean=((0.5, 1), (0.1, 0.6)),
                    stddev=((0.5, 1), (0.1, 0.5)),
                    height=(10, 11),
                    introduce_skew=True,
                )
            ],
            [
                GenericHillPhase(
                    hill_count=10,
                    mean=((0.5, 1), (0.1, 0.5)),
                    stddev=((0.5, 1), (0.1, 0.5)),
                    height=(10, 11),
                    introduce_skew=True,
                )
            ],
            False,
        ),
        (
            0,
            0,
            [
                GenericHillPhase(
                    hill_count=10,
                    mean=((0.5, 1), (0.1, 0.5)),
                    stddev=((0.5, 1), (0.1, 0.5)),
                    height=(10, 11),
                    introduce_skew=True,
                )
            ],
            [
                GenericHillPhase(
                    hill_count=10,
                    mean=((0.5, 1), (0.1, 0.5)),
                    stddev=((0.5, 1), (0.1, 0.5)),
                    height=(10, 11),
                    introduce_skew=False,
                )
            ],
            False,
        ),
    ),
)
def test_hashes_match(seed_a, seed_b, phases_a, phases_b, expected):
    config_a = HillGeneratorConfig(
        cache=True, random=PhasedRandomGenerator.create(seed_a)
    )
    config_b = HillGeneratorConfig(
        cache=True, random=PhasedRandomGenerator.create(seed_b)
    )

    generator_a = HillGenerator(config=config_a, phases=phases_a)
    generator_b = HillGenerator(config=config_b, phases=phases_b)

    assert (generator_a.hash == generator_b.hash) == expected


def test_hillgen_doesnt_use_cache_for_different_configs(random_seed):
    config_a = HillGeneratorConfig(
        cache=True, random=PhasedRandomGenerator.create(random_seed)
    )
    config_b = HillGeneratorConfig(
        cache=True, random=PhasedRandomGenerator.create(random_seed)
    )

    hillgen_a = HillGenerator(
        config_a,
        phases=[
            GenericHillPhase(
                hill_count=1,
                mean=((0.5, 1), (0.1, 0.5)),
                stddev=((0.5, 1), (0.1, 0.5)),
                height=(10, 11),
                introduce_skew=True,
            )
        ],
    )
    hillgen_b = HillGenerator(
        config_b,
        phases=[
            GenericHillPhase(
                hill_count=3,
                mean=((0.5, 1), (0.1, 0.5)),
                stddev=((0.5, 1), (0.1, 0.5)),
                height=(10, 11),
                introduce_skew=True,
            )
        ],
    )

    stage_a_heightfield = hillgen_a.create_stage()
    stage_b_heightfield = hillgen_b.create_stage()

    assert np.any(stage_a_heightfield != stage_b_heightfield)


def test_hillgen_uses_cache(random_seed):
    config_a = HillGeneratorConfig(
        cache=True, random=PhasedRandomGenerator.create(random_seed)
    )
    config_b = HillGeneratorConfig(
        cache=True, random=PhasedRandomGenerator.create(random_seed)
    )

    phase = GenericHillPhase(
        hill_count=1,
        mean=((0.5, 1), (0.1, 0.5)),
        stddev=((0.5, 1), (0.1, 0.5)),
        height=(10, 11),
        introduce_skew=True,
    )
    hillgen_a = HillGenerator(config_a, phases=[phase])
    hillgen_b = HillGenerator(config_b, phases=[phase])

    stage_a_heightfield = hillgen_a.create_stage()

    def broken_update_stage(*args, **kwargs):
        raise ValueError()

    object.__setattr__(phase, "update_stage", broken_update_stage)

    stage_b_heightfield = hillgen_b.create_stage()

    assert np.all(stage_a_heightfield == stage_b_heightfield)
