import math

import numpy as np
import pytest

from landscapegen.utils import math_utils


@pytest.fixture
def random_state() -> np.random.RandomState:
    return np.random.RandomState(42)


def test_uniform_constant(random_state: np.random.RandomState):
    generator = math_utils.UniformNumber((5, 5))
    assert generator(random_state) == 5


def test_uniform_non_constant(random_state: np.random.RandomState):
    generator = math_utils.UniformNumber((1, 10))
    for ii in range(100):
        num = generator(random_state)
        assert 1 <= num <= 10


def test_uniform_point_constant(random_state: np.random.RandomState):
    generator = math_utils.UniformPosition(((5, 5), (10, 10)))
    assert generator(random_state) == (5, 10)


def test_uniform_point_non_constant(random_state: np.random.RandomState):
    generator = math_utils.UniformPosition(((0, 10), (100, 110)))
    for ii in range(100):
        num = generator(random_state)
        assert 0 <= num[0] <= 10
        assert 100 <= num[1] <= 110


def test_gauss_almost_constant(random_state: np.random.RandomState):
    generator = math_utils.GaussianNumber(10, 0.001)
    for ii in range(100):
        num = generator(random_state)
        assert 9.99 <= num <= 10.01


def test_gauss_point_almost_constant(random_state: np.random.RandomState):
    generator = math_utils.GaussianPosition((5, 10), (0.001, 10))
    points = np.asarray([generator(random_state) for _ in range(100)])
    assert np.all((points[:, 0] > 4.99) & (points[:, 0] < 5.01))
    assert np.any((points[:, 1] < 5) | (points[:, 1] > 15))


def test_gauss_point_almost_constant_2(random_state: np.random.RandomState):
    generator = math_utils.GaussianPosition((5, 10), (10, 0.001))
    points = np.asarray([generator(random_state) for _ in range(100)])
    assert np.all((points[:, 1] > 9.99) & (points[:, 1] < 10.01))
    assert np.any((points[:, 0] < 0) | (points[:, 0] > 10))


def test_gauss_point_almost_constant_3(random_state: np.random.RandomState):
    generator = math_utils.GaussianPosition((5, 10), (0.001, 0.001))
    points = np.asarray([generator(random_state) for _ in range(100)])
    assert np.all((points[:, 0] > 4.99) & (points[:, 0] < 5.01))
    assert np.all((points[:, 1] > 9.99) & (points[:, 1] < 10.01))
    assert np.all(np.abs(np.cov(points.T)) < 0.01)


def test_gauss_non_constant(random_state: np.random.RandomState):
    generator = math_utils.GaussianNumber(10, 5)
    points = np.asarray([generator(random_state) for _ in range(10000)])
    mean = np.mean(points)
    stddev = np.std(points)
    assert 9.95 < mean < 10.05
    assert 4.95 < stddev < 5.05


def test_gauss_point_non_constant(random_state: np.random.RandomState):
    generator = math_utils.GaussianPosition((5, 10), (3, 5))
    points = np.asarray([generator(random_state) for _ in range(10000)])
    mean = np.mean(points[:, 0])
    stddev = np.std(points[:, 0])
    assert 4.95 < mean < 5.05
    assert 2.95 < stddev < 3.05
    mean = np.mean(points[:, 1])
    stddev = np.std(points[:, 1])
    assert 9.95 < mean < 10.05
    assert 4.95 < stddev < 5.05
    cov = np.cov(points.T, rowvar=True)
    assert 2.9 < np.sqrt(cov[0, 0]) < 3.1
    assert 4.9 < np.sqrt(cov[1, 1]) < 5.1
    assert np.abs(cov[0, 1]) < 0.1
    assert np.abs(cov[1, 0]) < 0.1


def test_gauss_point_almost_constant_skew(random_state: np.random.RandomState):
    generator = math_utils.GaussianPosition(
        (10, 10), (1, 10), rotate_radians=math.pi / 4
    )
    points = np.asarray([generator(random_state) for _ in range(10000)])
    cov = np.cov(points.T, rowvar=True)
    assert np.sqrt(np.abs(cov[0, 1])) > 0.5
    assert np.sqrt(np.abs(cov[1, 0])) > 2
