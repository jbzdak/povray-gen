import pathlib

import povraygen
from landscapegen.executor import SerialExecutor
from landscapegen.generator.abstract import PovrayOptions

scene = [
    povraygen.PointLightSource(location=[1000, 1000, -1000], color="White"),
    povraygen.Camera(location=[0, 0, 2], angle=60, look_at=[0, 0, 0]),
    #     povraygen.LiteralWithProps(
    #         name="isosurface",
    #         literal="""
    # function { x + z }
    # open
    # max_gradient 4
    # contained_by { box { -2, 2 } }
    #         """.strip(),
    #     pigment="Red",
    #     rotate=[dto.Vector(360, 0, 0),],
    # ),
    # povraygen.Cone(
    #     base_point=0,
    #     base_radius=.1,
    #     cap_point=[0, 1, 0],
    #     cap_radius=0,
    #     scale=[0.000001, 1, 1],
    #     pigment="Red",
    # ),
    povraygen.LiteralEscapeHatch(
        """

    #declare Rnd_1 = seed (1154);
    #declare R1 = 0.25 ; // ball radius
    #declare RR = 1.00 ; // ring radius
    #declare Position_1 =  <0,0,0> ;
    #declare Ball_Texture =
     texture{ pigment{ color rgb<1,0.65,0>}
              finish { phong 1.0 }
            } // end of texture
    #declare Ball1 =
    cone{
        <0, -.25, 0>
        0.1
        <0, .25 , 0>
        0
        scale <0.00001, 1, 1>
             texture{Ball_Texture}
             }
    //-------------------------------------
    object {
        Ball1    
        rotate <0, -90, 0>
        rotate <15, 0, 15>
        translate <-.1, 0, 0>
    }
    object {
        Ball1    
        rotate <0, 5, 0>
        translate <0, 0, 0>
    }
    object {
        Ball1    
        rotate <0, 90, 0>
        translate <.1, 0, 0>
    }
        """
    ),
]

file = povraygen.POVFile(
    preamble="""   
// -w320 -h240
// -w800 -h600 +a0.3
#version 3.7;
global_settings { assumed_gamma 1.5 }

#include "colors.inc"
""",
    contents=scene,
)

GENERATOR = povraygen.PovRaygenGenerator.create(file)

DEFAULT_OPTIONS = PovrayOptions(
    output_dir=pathlib.Path(f"out/povray-debug"), output_file_pattern="image-test.png",
)

SEEDS_TO_TRY = [None]
if __name__ == "__main__":
    executor = SerialExecutor()
    executor.execute(
        GENERATOR, SEEDS_TO_TRY, DEFAULT_OPTIONS,
    )
