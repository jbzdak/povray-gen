from color_palettes import color_brewer

from landscapegen.color import RandomPalette, ColorPalette

DIVERGING_BREWER_PALETTES = (
    "Spectral",
    "BrBG",
    "PiYG",
    "PRGn",
    "PuOr",
    "RdBu",
    "RdGy",
    "RdYlBu",
    "RdYlGn",
)


def brewer_diverging_palettes(colors_per_palette: int = 11) -> RandomPalette:
    assert 3 <= colors_per_palette <= 11
    return RandomPalette(
        tuple(
            ColorPalette(
                {"all": color_brewer.COLOR_BREWER[palette].palettes[colors_per_palette]}
            )
            for palette in DIVERGING_BREWER_PALETTES
        )
    )
