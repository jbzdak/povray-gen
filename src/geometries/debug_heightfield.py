import pathlib
from typing import List

import povraygen
from landscapegen.color import ConstantColor
from landscapegen.executor import SerialExecutor, TimedExecutor
from landscapegen.generator.abstract import PovrayOptions
from landscapegen.hill_generator import HillGenerator, ArrayBackedHeightfield
from landscapegen.misc import SunGenerator
from landscapegen.utils.abstract import PhasedRandomGenerator
from povraygen import Translate, Scale, Rotate

slow = False

scene: List[povraygen.POVRayObject] = [
    povraygen.LiteralEscapeHatch(f"#declare RAD = {'on' if slow else 'off'};"),
    povraygen.LiteralEscapeHatch(
        """
global_settings {
assumed_gamma 1.5 
# if(RAD)
radiosity {
  pretrace_start 0.08
  pretrace_end   0.01
  count 150
  nearest_count 10
  error_bound 0.5
  recursion_limit 3
  low_error_factor 0.5
  gray_threshold 0.0
  minimum_reuse 0.005
  maximum_reuse 0.2
  brightness 1
  adc_bailout 0.005
}
# end
}
"""
    ),
]

main_generator = PhasedRandomGenerator.create(10)

# hill_generator = HillGenerator.default_generator(
#     main_generator.create_phase(), cache=True
# )
hill_generator = HillGenerator.debug_generator()
stage_heights = hill_generator.create_stage()


# def make_landscape():
#     size = 1000
#     coord = np.arange(size)
#     landscape = np.zeros((size, size), dtype=float)
#     landscape[:, :] = coord[:, np.newaxis] + coord[np.newaxis, :]
#     return ArrayBackedHeightfield.scale_array(landscape)
# stage_heights = make_landscape()

sun_generator = SunGenerator(
    light_color=ConstantColor("White"), ball_color=ConstantColor("White"),
)

sun = sun_generator(main_generator.create_phase().generator)

scene.extend([sun])

height_field = ArrayBackedHeightfield(
    height_field=stage_heights,
    archetype=povraygen.HeightField(
        height_field="/to/be/initialized", pigment="DarkGreen", smooth=False,
    ),
)

scene.append(height_field)

camera_position = [-0.3, 0.1, -0.3]
look_at = [1, 0.0, 1]


def make_dots():
    for ii in range(0, 1000, 3):
        for jj in range(0, 1000, 3):
            # yield povraygen.Sphere(
            #     location=height_field.get_coordinates_indices(ii, jj, 0),
            #     radius=0.001,
            #     pigment="Yellow",
            #     # transformations=[
            #     #     Rotate(*height_field.position_rotation[ii, jj, :])
            #     # ]
            # )
            yield povraygen.Cone(
                base_point=[0, 0, 0],
                base_radius=0.001,
                cap_point=[0, 0.005, 0],
                cap_radius=0,
                pigment="Yellow",
                transformations=[
                    Rotate(*height_field.position_rotation[ii, jj, :]),
                    Translate(*height_field.get_coordinates_indices(ii, jj, 0)),
                ],
            )


scene.append(make_dots())

print(camera_position)

scene.extend(
    (
        #
        povraygen.Camera(location=camera_position, angle=30, look_at=look_at),
    )
)

scene.append(
    povraygen.SkySphere(
        pigment=povraygen.Pigment(
            gradient=[0, 1, 0],
            color_map=[
                povraygen.ColorMapElement(0, "White"),
                povraygen.ColorMapElement(0.8, povraygen.RGB(0.1, 0.25, 0.75)),
                povraygen.ColorMapElement(1, povraygen.RGB(0.1, 0.25, 0.75)),
            ],
            transformations=[Translate(0, -0.5, 0), Scale(2, 2, 2)],
        )
    )
)

scene.append(povraygen.Cylinder.horizontal_marker(1, 1, 0.01, height=1000))


file = povraygen.POVFile(
    preamble="""   
// -w320 -h240
// -w800 -h600 +a0.3
#version 3.7;    
#include "colors.inc"
""",
    contents=scene,
)

GENERATOR = povraygen.PovRaygenGenerator.create(file)

DEFAULT_OPTIONS = PovrayOptions(
    output_dir=pathlib.Path(f"out/povray-debug"),
    output_file_pattern="image-debug.png",
    image_size=9000,
)

SEEDS_TO_TRY = [None]


if __name__ == "__main__":
    executor = TimedExecutor(SerialExecutor())
    executor.execute(
        GENERATOR, SEEDS_TO_TRY, DEFAULT_OPTIONS,
    )
