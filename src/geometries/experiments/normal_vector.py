import pathlib

import numpy as np

import povraygen
from landscapegen.executor import SerialExecutor
from landscapegen.generator.abstract import PovrayOptions
from landscapegen.hill_generator import ArrayBackedHeightfield
from povraygen import dto, Translate, Scale

THIS = pathlib.Path(__file__)


def make_landscape():
    size = 1000
    coord = np.arange(size)
    landscape = np.zeros((size, size), dtype=float)
    landscape[:, :] = coord[:, np.newaxis] + coord[np.newaxis, :]
    return ArrayBackedHeightfield.scale_array(landscape)


LANDSCAPE = make_landscape()

HEIGHT_FIELD = ArrayBackedHeightfield(
    height_field=LANDSCAPE,
    archetype=povraygen.HeightField(
        height_field="/to/be/filled",
        pigment="DarkGreen",
        transformations=[povraygen.Scale(1, 1, 1)],
        smooth=False,
    ),
)


scene = [
    povraygen.PointLightSource(location=[1000, 1000, -1000], color="White"),
    HEIGHT_FIELD,
    povraygen.SkySphere(
        pigment=povraygen.Pigment(
            gradient=[0, 1, 0],
            color_map=[
                povraygen.ColorMapElement(0, "White"),
                povraygen.ColorMapElement(0.8, povraygen.RGB(0.1, 0.25, 0.75)),
                povraygen.ColorMapElement(1, povraygen.RGB(0.1, 0.25, 0.75)),
            ],
            transformations=[Translate(0, -0.5, 0), Scale(2, 2, 2)],
        )
    ),
    povraygen.Camera(location=[0.7, 0.5, 0], angle=30, look_at=[0.5, 0.1, 0.5]),
    # povraygen.Camera(location=[1, .8, .5], angle=120, look_at=[.5, .5, .5]),
    # povraygen.Camera(location=[1, .8, .5], angle=120, look_at=[.5, .5, .5]),
    povraygen.DeclareObject(
        "SPHERE",
        dto.Cone(
            base_point=povraygen.Vector(0, 0, 0),
            base_radius=0.001,
            cap_point=povraygen.Vector(0, 0.005, 0),
            cap_radius=0,
            pigment="Yellow",
            transformations=[povraygen.Rotate(-26, 0, 26)],
        ),
    ),
]


for coord_x in range(0, 1000, 100):
    for coord_y in range(0, 1000, 100):

        scene.append(
            dto.TransformedObject(
                referenced_object="SPHERE",
                transformations=[
                    Translate(
                        *HEIGHT_FIELD.get_coordinates_indices(coord_x, coord_y, 0.001)
                    )
                ],
            )
        )

file = povraygen.POVFile(
    preamble="""   
#version 3.7;
#include "colors.inc"
global_settings { 
    assumed_gamma 1.5
    ambient_light White 
}

""",
    contents=scene,
)

GENERATOR = povraygen.PovRaygenGenerator.create(file)

DEFAULT_OPTIONS = PovrayOptions(
    output_dir=pathlib.Path(f"/tmp/povray-debug"),
    output_file_pattern=f"{THIS.stem}.png",
    image_size=3000,
)

SEEDS_TO_TRY = [None]
if __name__ == "__main__":
    executor = SerialExecutor()
    executor.execute(
        GENERATOR, SEEDS_TO_TRY, DEFAULT_OPTIONS,
    )
