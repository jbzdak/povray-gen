from typing import Sequence, Tuple

from color_palettes.seaborn_copy.palettes import (
    hls_palette,
    SeabornColorPalette,
)
from color_palettes.utils import Color
from landscapegen.color import ColorPalette, WeightedPalette

BASE_PALETTE_SIZE = 24


def add_weights(
    colors: Sequence[Color], weight: float, item_weight=True
) -> Sequence[Tuple[Color, int]]:
    if not item_weight:
        weight /= len(colors)
    return [(Color.from_rgb(color, rgb_scale=1), weight) for color in colors]


def color_palette_attempt_1() -> ColorPalette:

    base_categories = {}

    dark_colors = hls_palette(1, s=1, l=0.0, h=1)
    dark_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=1, l=0.1, h=1))
    dark_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=1, l=0.2, h=1))

    gray_colors = hls_palette(1, s=0.0, l=0.7, h=1)
    gray_colors.extend(hls_palette(1, s=0.0, l=0.8, h=1))
    gray_colors.extend(hls_palette(1, s=0.0, l=0.6, h=1))
    gray_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=0.1, l=0.6, h=1))
    gray_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=0.2, l=0.6, h=1))
    gray_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=0.4, l=0.6, h=1))

    vivid_colors = SeabornColorPalette()

    vivid_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=1, l=0.5, h=1))
    vivid_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=0.8, l=0.5, h=1))
    vivid_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=0.7, l=0.5, h=1))
    vivid_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=0.6, l=0.5, h=1))

    vivid_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=1, l=0.6, h=1))
    vivid_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=1, l=0.65, h=1))

    vivid_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=0.9, l=0.6, h=1))
    vivid_colors.extend(hls_palette(BASE_PALETTE_SIZE, s=0.8, l=0.6, h=1))

    base_categories["dark"] = add_weights(dark_colors, 3, False)
    base_categories["gray"] = add_weights(gray_colors, 1, False)
    base_categories["vivid"] = add_weights(vivid_colors, 10, False)

    derived_categories = {
        "sun": ["vivid"],
        "sky": ["vivid"],
        "ground": ["vivid"],
        "grass": ["vivid"],
    }

    return WeightedPalette.create(base_categories, derived_categories)
