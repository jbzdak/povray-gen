import hashlib
import itertools
import logging
import math
import pathlib
import pickle
import random
import typing
from typing import Optional, List, Sequence, Iterator, Tuple

import attr
import numpy as np
from scipy.stats import multivariate_normal

import povraygen
from landscapegen.utils import Num, XYCoord, CoordBounds, PointRandomBounds
from landscapegen.utils.abstract import PhasedRandomGenerator, SEED_SIZE
from povraygen import POVRayObject
from povraygen.utils import CACHE_DIR

LOGGER = logging.getLogger("hill_generator")


def freeze_xy(to_freeze: typing.Sequence[Num]) -> XYCoord:
    assert len(to_freeze) == 2
    return to_freeze[0], to_freeze[1]


def make_hill(
    point_matrix, center: typing.Any, stddev_matrix: typing.Any, hill_height: float
) -> np.ndarray:
    rv = multivariate_normal(center, stddev_matrix)
    hill = rv.pdf(point_matrix)
    return hill / np.max(hill) * hill_height


def random_coordinate(random_gen: random.Random, bounds: CoordBounds) -> Num:
    if isinstance(bounds, (int, float)):
        return bounds
    return random_gen.uniform(*bounds)


def seed_converter(seed: Optional[int]):
    if seed is None:
        return 42
    return seed


@attr.dataclass(frozen=True)
class HillGeneratorConfig:
    random: PhasedRandomGenerator
    cache: bool = False
    stage_size: XYCoord = (1, 1)
    stage_resolution: XYCoord = (1000, 1000)
    collect_stage_stack: bool = False


@attr.dataclass(frozen=True)
class PhaseContext:
    config: HillGeneratorConfig
    point_positions: np.ndarray
    phase_generator: random.Random

    stage_stack: Optional[List[np.ndarray]] = None


def validate_heightfield(instance: object, attrib, value: np.ndarray):

    # NOTE: If you edit this function to allow more transformations you
    # will need to update __attrs_post_init__

    assert isinstance(value, np.ndarray)
    assert value.dtype == np.dtype(
        "uint16"
    ), "np.array passed to HeightField needs to be uint16 array. Maybe call HeightField.scale_array"
    assert len(value.shape) == 2
    assert (
        value.shape[0] == value.shape[1]
    ), "Only square arrays are OK for heightfields ATM"


def validate_transformations(value: Optional[Sequence[povraygen.Transformation]]):

    # NOTE: If you edit this function to allow more transformations you
    # will need to update __attrs_post_init__

    if value is None:
        return
    transformations = value
    assert (
        len(transformations) <= 1
    ), "only height fields with a single scale transformations are supported"

    if len(transformations) == 0:
        return

    transformation = transformations[0]

    assert isinstance(
        transformation, povraygen.Scale
    ), "only height fields with a single scale transformations are supported"

    assert (
        transformation.get_x() == 1 and transformation.get_z() == 1
    ), "scale transformation with x and z equal to one are supported"


@attr.dataclass(frozen=True)
class ArrayBackedHeightfield(povraygen.CompositeObject):

    height_field: np.ndarray = attr.ib(validator=validate_heightfield)

    archetype: povraygen.HeightField = attr.ib()

    hash: typing.Optional[str] = attr.ib(default=None)

    vertical_scale: Num = attr.ib(
        init=False, repr=False, hash=False, eq=False, default=None,
    )

    # Distance between two neighboring elements of the array in the
    # units used by the povray generator.
    horizontal_resolution: Num = attr.ib(
        init=False, repr=False, hash=False, eq=False, default=None,
    )

    max: Optional[Num] = attr.ib(
        init=False, repr=False, hash=False, eq=False, default=None,
    )

    position_rotation: np.ndarray = attr.ib(
        init=False, repr=False, hash=False, eq=False, default=None,
    )
    """
    Array containing (x, y, z) rotion vectors, for each pixel at 
    height_field. Each rotation vector will rotate vector (0, 1, 0) to
    to normal vector (vector perpendicular to the sufrace) at a given position. 
    
    Vectors are already in degress, so can be directly consumed by povray. 
        
    '"""

    @classmethod
    def __compute_rotation_angles(
        cls, stage: np.ndarray, resolution: int, vertical_scale: float
    ):
        """
        :param stage: N
        :param resolution:
        :param vertical_scale:
        :return:
        """
        stage = cls.scale_array(
            stage, scale_to=vertical_scale, output_dtype=np.float128, owns_array=False
        )
        gradz, gradx = np.gradient(stage * resolution)
        new_shape = (stage.shape[0], stage.shape[1], 3)
        angles = np.zeros(new_shape, dtype=np.float64)
        angles[:, :, 0] = -(np.arctan(gradx) / math.pi * 180)
        angles[:, :, 2] = np.arctan(gradz) / math.pi * 180
        return angles

    def __attrs_post_init__(self):
        validate_transformations(self.archetype.transformations)
        scale = 1
        if len(self.archetype.transformations) > 0:
            scale = self.archetype.transformations[0].get_y()
        object.__setattr__(self, "vertical_scale", scale)
        object.__setattr__(self, "horizontal_resolution", self.height_field.shape[0])
        object.__setattr__(self, "max", np.max(self.height_field))
        object.__setattr__(
            self,
            "position_rotation",
            self.__compute_rotation_angles(
                stage=self.height_field,
                resolution=self.horizontal_resolution,
                vertical_scale=scale,
            ),
        )

    def get_coordinates_indices(
        self, index_x: Num, index_z: Num, y_delta: float = 0
    ) -> Optional[Tuple[float, float, float]]:
        """
        Returns coordinate at index [x, y].
        """
        position_x = index_x / self.height_field.shape[0]
        position_z = index_z / self.height_field.shape[1]
        try:
            position_y = (
                self.height_field[int(index_x), int(index_z)]
                / self.max
                * self.vertical_scale
                + y_delta
            )
            return position_x, position_y, position_z
        except IndexError:
            return None

    def __iter__(self) -> Iterator[POVRayObject]:
        yield attr.evolve(
            self.archetype, height_field_hash=self.hash, height_field=self.height_field,
        )

    @classmethod
    def scale_array(
        cls,
        array: np.ndarray,
        *,
        scale_to: Num = (2 ** 16 - 1),
        output_dtype: np.dtype = np.uint16,
        owns_array: bool = False,
    ) -> np.ndarray:
        """
        Creates new array of type uint16 that uses whole spectrum between 0 and 2^16-1.

        :param array:
        :param scale_to:
        :param output_dtype:
        :param owns_array: If False won't modify array, if True array will be
                               in undefined state after call.


        We only care about relative differences between points,
        so the below arrays will produce the same scaled arrays.

        >>> ArrayBackedHeightfield.scale_array(np.arange(5))
        array([    0, 16383, 32767, 49151, 65535], dtype=uint16)
        >>> ArrayBackedHeightfield.scale_array(np.linspace(0, .5, 5))
        array([    0, 16383, 32767, 49151, 65535], dtype=uint16)

        >>> ArrayBackedHeightfield.scale_array(np.arange(5),scale_to=1,output_dtype=np.float64)
        array([0.  , 0.25, 0.5 , 0.75, 1.  ])

        >>> ArrayBackedHeightfield.scale_array(np.arange(5),scale_to=10,output_dtype=np.float64)
        array([ 0. ,  2.5,  5. ,  7.5, 10. ])


        Check owns_parameter behaviour

        >>> initial = np.linspace(0, .5, 5, dtype=np.float64)
        >>> comparision = np.array(initial, copy=True)
        >>> comparision is not initial
        True
        >>> np.all(initial == comparision)
        True

        When function does not take ownership of the parameter
        it will not modify the passed array.

        >>> ArrayBackedHeightfield.scale_array(initial,owns_array=False)
        array([    0, 16383, 32767, 49151, 65535], dtype=uint16)
        >>> np.all(initial == comparision)
        True

        When it owns it might modify the array.

        >>> ArrayBackedHeightfield.scale_array(initial,owns_array=True)
        array([    0, 16383, 32767, 49151, 65535], dtype=uint16)
        >>> np.all(initial == comparision)
        False

        """
        assert scale_to > 0
        assert scale_to > 1 or output_dtype in {
            np.float,
            np.float32,
            np.float64,
            np.float128,
            float,
        }
        dtype = np.float64

        if array.dtype == np.float128:
            dtype = np.float128

        new_array = np.array(array, dtype=dtype, copy=(not owns_array), subok=True)
        new_array -= np.min(new_array)
        max = np.max(new_array)
        if max != 0:
            new_array /= max
        new_array *= scale_to
        return new_array.astype(output_dtype, copy=False, subok=True)


class GenerationPhase:
    @classmethod
    def make_random_hill(
        cls,
        context: PhaseContext,
        mean_bounds: PointRandomBounds,
        stddev_bounds: PointRandomBounds,
        height_bounds: CoordBounds,
        introduce_skew: bool,
        attempts: int = 100,
        raise_on_error: bool = False,
    ) -> np.ndarray:

        random_gen = random.Random(context.phase_generator.getrandbits(SEED_SIZE))

        mean: List[Num] = [
            random_coordinate(random_gen, mean_bounds[0]),
            random_coordinate(random_gen, mean_bounds[1]),
        ]

        # This array describes blob that is not
        stddev: List[List[Num]] = [
            [random_coordinate(random_gen, stddev_bounds[0]), 0],
            [0, random_coordinate(random_gen, stddev_bounds[1])],
        ]

        height = random_coordinate(random_gen, height_bounds)

        # print(mean, stddev, height)
        if not introduce_skew:
            return make_hill(context.point_positions, mean, stddev, height)

        # This is idiotic, but I don't have the time or willpower to brush on my
        # math skills to do it properly.
        # The idea is that hill from matrix with  [0, 1] and [1, 0] set to zero
        # hase ridge perpendicular to X or Y axis, which doesn't make sense.
        # So I introduce skew there, but these values can't be completely random
        # as matrix needs to be semidefinite (whatever that means).
        # I fiddled with notebook for 15 minutes and the below introduces interesting
        # hills and does not raise a lot of exceptions.
        for attempt in range(attempts):
            stddev[0][1] = random_gen.uniform(-abs(stddev[1][1]), +abs(stddev[1][1]))
            stddev[1][0] = random_gen.uniform(-abs(stddev[0][0]), +abs(stddev[0][0]))
            try:
                return make_hill(context.point_positions, mean, stddev, height)
            except ValueError as e:
                if attempt == attempts - 1 and raise_on_error:
                    raise ValueError(mean, stddev, height) from e
        return np.zeros(context.config.stage_resolution)

    @classmethod
    def _update_stage(cls, context: PhaseContext, stage, hill):
        stage += hill
        if context.stage_stack is not None:
            context.stage_stack.append(hill)

    def update_stage(self, context: PhaseContext, stage: np.ndarray):
        raise NotImplementedError


@attr.dataclass(frozen=True)
class GenericHillPhase(GenerationPhase):
    hill_count: int
    mean: PointRandomBounds
    stddev: PointRandomBounds
    height: CoordBounds
    introduce_skew: bool = True
    attempts_per_hill: int = 100
    raise_on_error: bool = False

    def update_stage(self, context: PhaseContext, stage: np.ndarray):

        for _ in range(self.hill_count):
            hill = self.make_random_hill(
                context=context,
                mean_bounds=self.mean,
                stddev_bounds=self.stddev,
                height_bounds=self.height,
                introduce_skew=self.introduce_skew,
                attempts=self.attempts_per_hill,
                raise_on_error=self.raise_on_error,
            )

            stage += hill
            self._update_stage(context, stage, hill)


@attr.dataclass(frozen=True)
class ConstantHillPhase(GenerationPhase):
    mean: XYCoord = attr.ib(converter=freeze_xy)
    stddev: XYCoord = attr.ib(converter=freeze_xy)
    height: Num

    def update_stage(self, context: PhaseContext, stage: np.ndarray):
        stddev = [[self.stddev[0], 0], [0, self.stddev[1]]]

        hill = make_hill(
            context.point_positions, self.mean, stddev, hill_height=self.height
        )
        self._update_stage(context, stage, hill)


@attr.dataclass
class HillGenerator:
    config: HillGeneratorConfig
    phases: Sequence[GenerationPhase]
    stage_stack: List[np.ndarray] = attr.ib(factory=list)

    point_positions: np.ndarray = None

    hash: Optional[str] = None

    def __attrs_post_init__(self):
        x_res = self.config.stage_resolution[0]
        y_res = self.config.stage_resolution[1]
        point_positions = np.zeros((x_res, y_res, 2))
        point_positions[:, :, 0] = np.linspace(0, 1, x_res)[:, np.newaxis]
        point_positions[:, :, 1] = np.linspace(0, 1, y_res)[np.newaxis, :]
        self.point_positions = point_positions
        phases_hash = hashlib.sha256(
            pickle.dumps((self.phases, self.config))
        ).hexdigest()
        self.hash = f"{self.config.random.initial_seed}.{phases_hash}"

    def create_stage(self) -> np.ndarray:

        if self.config.cache:
            cached = self.__read_cache()
            if cached is not None:
                return cached

        stage = np.zeros(self.config.stage_resolution, dtype=np.float128)

        self.stage_stack.clear()

        for phase in self.phases:
            context = PhaseContext(
                config=self.config,
                point_positions=self.point_positions,
                phase_generator=random.Random(
                    self.config.random.phase_generator.getrandbits(128)
                ),
                stage_stack=self.stage_stack
                if self.config.collect_stage_stack
                else None,
            )
            phase.update_stage(context=context, stage=stage)

        # Convert float array to unit16 one that uses whole available spectrum
        # (from 0 to 2 ** 16 -1)
        stage = ArrayBackedHeightfield.scale_array(stage, owns_array=True)

        if self.config.cache:
            self.__write_cache(stage)

        return stage

    @classmethod
    def __cache_dir(cls):
        cache_dir = CACHE_DIR / "hillgen_cache"
        cache_dir.mkdir(exist_ok=True)
        return cache_dir

    def __cache_file(self) -> pathlib.Path:

        return self.__cache_dir() / f"{self.hash}.bin"

    def __write_cache(self, stage: typing.Any):
        with self.__cache_file().open("wb") as f:
            try:
                pickle.dump(stage, f, protocol=pickle.HIGHEST_PROTOCOL)
            except:
                self.__cache_file().unlink()
                raise

    def __read_cache(self) -> Optional[typing.Any]:
        if self.__cache_file().exists():
            with self.__cache_file().open("rb") as f:
                return pickle.load(f)
        return None

    @classmethod
    def debug_generator(cls, **kwargs):
        config = HillGeneratorConfig(
            random=PhasedRandomGenerator.create(seed=0), cache=True, **kwargs
        )
        hills = np.arange(-0.1, 1.2, 0.1)
        sigma = 0.0001
        phases = []

        for x, y in itertools.product(hills, hills):
            phases.append(ConstantHillPhase([x, y], [sigma, sigma], (x + y) * 10))

        return HillGenerator(config=config, phases=phases)

    @classmethod
    def default_generator(
        cls, generator: PhasedRandomGenerator = None, cache: bool = True, **kwargs
    ):
        if generator is None:
            generator = PhasedRandomGenerator.create()
        config = HillGeneratorConfig(generator, cache=cache, **kwargs)
        return HillGenerator(
            config=config,
            phases=[
                # Large hill phase large hills that go mostly from east to west
                # that look something, something like post glacial landscape??
                GenericHillPhase(
                    hill_count=15,
                    mean=((-0.3, 1.3), (-0.1, 1.1)),
                    stddev=((0.001, 0.1), (0.6, 3)),
                    height=(1, 2),
                    introduce_skew=True,
                ),
                # Small hills higher but smaller, and without any direction skew.
                GenericHillPhase(
                    hill_count=35,
                    mean=((-0.1, 1.1), (-0.1, 1.1)),
                    stddev=((0.006, 0.02), (0.006, 0.02)),
                    height=(1, 2),
                    introduce_skew=True,
                ),
            ],
        )
