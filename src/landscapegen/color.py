import abc
import itertools
from collections import defaultdict
from operator import itemgetter
from typing import Mapping, Sequence, Optional, Dict, Union, Tuple, List

import attr
import numpy as np
from numpy.random.mtrand import RandomState

from color_palettes.utils import Color
from landscapegen.utils.context import get_context
from povraygen import ColorDefinition
from povraygen.dto import Num
from povraygen.utils import freeze_ndarray, freeze


@attr.dataclass
class ColorContext:
    palette: Optional["ColorPalette"] = attr.ib(
        kw_only=True, default=None,
    )


@attr.dataclass(frozen=False)
class ColorPaletteFactory:
    colors: Dict[str, Sequence[Color]] = attr.ib(factory=lambda: defaultdict(list))

    def add_sub_palette(self, name: str, colors: Sequence[Color]):
        self.colors[name] = colors

    def create(self) -> "ColorPalette":
        return SimplePalette(colors=self.colors,)


class ColorPalette(abc.ABC):
    @abc.abstractmethod
    def get_random_color(
        self,
        random: RandomState,
        category: Optional[Union[str, Sequence[str]]] = tuple(),
    ) -> Color:
        raise NotImplementedError


@attr.dataclass(frozen=True)
class SimplePalette(ColorPalette):
    colors: Mapping[str, Sequence[Color]]

    all_colors: Sequence[Color] = None  # type: ignore

    def __attrs_post_init__(self):
        object.__setattr__(self, "colors", freeze(self.colors))
        object.__setattr__(
            self, "all_colors", tuple(itertools.chain(*self.colors.values()))
        )

    def get_random_color(
        self,
        random: RandomState,
        category: Optional[Union[str, Sequence[str]]] = tuple(),
    ) -> Color:
        if isinstance(category, str):
            category = [category]
        if category is None:
            category = tuple()
        color_set = self.all_colors
        for cat in category:
            if cat in self.colors:
                color_set = self.colors[cat]
                break
        return color_set[random.randint(len(color_set))]


def weights_to_prob(obj: Union[np.ndarray, Sequence[Union[int, float]]]) -> np.ndarray:

    obj = np.asarray(obj, dtype=float)
    obj /= obj.sum()
    return obj


@attr.dataclass(frozen=True)
class ColorCategoryDefinition:

    colors: Sequence[Color] = attr.ib(converter=freeze)
    color_weights: np.ndarray = attr.ib(converter=freeze_ndarray)
    color_probabilities: np.ndarray = None
    color_indices: np.ndarray = None

    def __attrs_post_init__(self):

        if self.color_indices is None:
            object.__setattr__(
                self, "color_indices", freeze_ndarray(np.arange(len(self.colors)))
            )
        object.__setattr__(
            self, "color_probabilities", self.color_weights / self.color_weights.sum()
        )
        assert len(self.colors) == len(self.color_indices)
        assert len(self.color_weights) == len(self.colors)

    def random_color(self, random: RandomState) -> Color:
        index = random.choice(self.color_indices, p=self.color_probabilities)
        result = self.colors[index]
        return result

    @classmethod
    def merge_definitions(cls, *definitions: "ColorCategoryDefinition"):
        colors: List[Color] = []
        weights: List[Num] = []
        for definition in definitions:
            colors.extend(definition.colors)
            weights.extend(definition.color_weights)

        return cls(colors=colors, color_weights=weights)


@attr.dataclass(frozen=True)
class WeightedPalette(ColorPalette):

    colors: Mapping[str, ColorCategoryDefinition] = attr.ib(converter=freeze)

    @classmethod
    def create(
        cls,
        colors_by_category: Mapping[str, Sequence[Tuple[Color, Num]]],
        category_mapping: Mapping[str, Sequence[str]],
    ):
        category_mapping = dict(**category_mapping)

        palette_map = {}

        for category, colors_with_weights in colors_by_category.items():

            colors = list(map(itemgetter(0), colors_with_weights))
            category_weights = np.asarray(
                list(map(itemgetter(1), colors_with_weights)), dtype=float
            )
            palette_map[category] = ColorCategoryDefinition(colors, category_weights)

        if "__all__" not in category_mapping:
            category_mapping["__all__"] = list(palette_map.keys())

        for new_category, parent_categories in category_mapping.items():
            palette_map[new_category] = ColorCategoryDefinition.merge_definitions(
                *[palette_map[parent] for parent in parent_categories]
            )

        return cls(colors=palette_map)

    def get_random_color(
        self,
        random: RandomState,
        category: Optional[Union[str, Sequence[str]]] = tuple(),
    ) -> Color:
        if category is None:
            category = tuple()
        for cat in tuple(category) + tuple("__all__",):
            if cat in self.colors:
                return self.colors[cat].random_color(random)
        raise ValueError


class RandomPalette:
    def __call__(self, generator: RandomState) -> ColorPalette:
        pass


@attr.dataclass(frozen=True)
class ConstantPalette(RandomPalette):

    palette: ColorPalette

    def __call__(self, generator: RandomState) -> ColorPalette:
        return self.palette


class RandomColor:
    @abc.abstractmethod
    def __call__(self, generator: RandomState) -> ColorDefinition:
        raise NotImplementedError


def string_to_tuple(val: Union[str, Sequence[str]]):
    if isinstance(val, str):
        return (val,)
    return tuple(val)


@attr.dataclass(frozen=True)
class RandomColorFromContextPalette:

    categories: Sequence[str] = attr.ib(converter=string_to_tuple, default=tuple())

    @abc.abstractmethod
    def __call__(self, generator: RandomState) -> ColorDefinition:
        ctx = get_context(ColorContext)
        assert ctx.palette is not None
        return ctx.palette.get_random_color(generator, self.categories)


@attr.dataclass()
class RandomColorFromRandomPalette(RandomColor):
    palettes: Sequence[ColorPalette]

    palette: ColorPalette = None  # type: ignore

    def initialize_from(self, generator: RandomState):
        self.palette = generator.choice(self.palettes)

    def __call__(self, generator: RandomState) -> Color:
        return generator.choice(self.palette)


@attr.dataclass(frozen=True)
class RandomColorFromPalette(RandomColor):
    palette: ColorPalette
    color_set: Optional[str] = None

    @classmethod
    def placeholder(cls) -> "RandomColor":
        class RandomColorPlaceholder(RandomColor):
            def __call__(self, *args, **kwargs):
                raise ValueError("Unfilled placeholder color")

        return RandomColorPlaceholder()

    def __call__(self, generator: RandomState) -> Color:
        return self.palette.get_random_color(generator, self.color_set)


@attr.dataclass(frozen=True)
class ConstantColor(RandomColor):

    color: ColorDefinition

    def __call__(self, generator: RandomState) -> ColorDefinition:
        generator.random()
        return self.color
