import abc
import math
import typing
from typing import Optional, Union

import attr
import numpy as np
from numpy.random.mtrand import RandomState

from landscapegen.utils import types
from povraygen import Vector

if typing.TYPE_CHECKING:
    pass


def round_cartesian(input: typing.Sequence):
    return list(map(lambda x: round(x, 2), input))


def sphere_to_cartesian(r: types.Num, alpha: types.Num, beta: types.Num):
    """
    >>> sphere_to_cartesian(1, 0, 0)
    [0.0, 0.0, 1.0]
    >>> round_cartesian(sphere_to_cartesian(1, math.pi / 2, 0))
    [0.0, 1.0, 0.0]
    >>> round_cartesian(sphere_to_cartesian(1, 0, math.pi / 2))
    [1.0, 0.0, 0.0]

    :param r:
    :param alpha:
    :param beta:
    :return:
    """

    return [
        r * math.cos(alpha) * math.sin(beta),
        r * math.sin(alpha),
        r * math.cos(alpha) * math.cos(beta),
    ]


class RandomNumber(abc.ABC):
    @abc.abstractmethod
    def __call__(self, rand: np.random.RandomState) -> types.Num:
        raise NotImplementedError


class PositionGenerator(abc.ABC):
    @abc.abstractmethod
    def make_position(self, rand: np.random.RandomState) -> "RandomPosition":
        raise NotImplementedError


class RandomPosition(PositionGenerator):
    @abc.abstractmethod
    def __call__(self, rand: np.random.RandomState) -> types.XYCoord:
        raise NotImplementedError

    def make_position(self, rand: np.random.RandomState):
        return self


@attr.dataclass(frozen=True)
class ConstantNumber(RandomNumber):

    number: types.Num

    def __call__(self, rand: np.random.RandomState) -> types.Num:
        return self.number


class ConstantPosition(RandomPosition):

    position: types.XYCoord

    def __call__(self, rand: np.random.RandomState) -> types.XYCoord:
        return self.position


@attr.dataclass(frozen=True)
class UniformNumber(RandomNumber):

    bounds: types.CoordBounds

    def __call__(self, rand: np.random.RandomState) -> types.Num:
        return rand.uniform(*self.bounds)


@attr.dataclass(frozen=True)
class UniformPosition(RandomPosition):

    bounds: types.PointRandomBounds

    def __call__(self, rand: np.random.RandomState) -> types.XYCoord:
        return rand.uniform(*self.bounds[0]), rand.uniform(*self.bounds[1])


@attr.dataclass(frozen=True)
class GaussianNumber(RandomNumber):

    mean: types.Num
    stddev: types.Num

    def __call__(self, rand: np.random.RandomState) -> types.Num:
        return rand.normal(self.mean, self.stddev)


@attr.dataclass(frozen=True)
class GaussianPosition(RandomPosition):
    """
    This is another attempt to solve this "positive semidefinite" covariance bullshit.

    Each point is calculated from independent normal distributions, and then optionally
    rotated. By setting up the rotations we might get anything we'd get from setting
    covariance, reason is something along the lines: "mumble mumble covariance mumble
    linear transformation mumble".
    """

    mean: types.XYCoord
    stddev: types.XYCoord
    rotate_radians: Optional[types.Num] = None

    rotate_matrix: np.ndarray = attr.ib(init=False, default=np.eye(2))

    def __attrs_post_init__(self):
        theta = self.rotate_radians
        if theta is not None:
            matrix = np.array(
                [[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]]
            )
            object.__setattr__(self, "rotate_matrix", matrix)

    def __call__(self, rand: np.random.RandomState) -> types.XYCoord:
        result = np.zeros(2, dtype=float)
        result[0] = rand.normal(0, self.stddev[0])
        result[1] = rand.normal(0, self.stddev[1])

        if self.rotate_radians is not None:
            result = np.dot(self.rotate_matrix, result)

        result[0] += self.mean[0]
        result[1] += self.mean[1]

        return result[0], result[1]


@attr.dataclass(frozen=True)
class GaussianPositionGenerator(PositionGenerator):
    mean: RandomPosition
    stddev: RandomPosition
    rotate_radians: Optional[Union[bool, RandomNumber]]

    def make_position(self, rand: np.random.RandomState) -> "GaussianPosition":
        return self(rand)

    def __call__(self, generator: np.random.RandomState) -> "GaussianPosition":
        rotate_radians = self.rotate_radians
        if isinstance(rotate_radians, bool):
            if rotate_radians:
                rotate_radians = UniformNumber((-math.pi / 2, +math.pi / 2))
            else:
                rotate_radians = ConstantNumber(0)
        if rotate_radians is None:
            rotate_radians = ConstantNumber(0)
        return GaussianPosition(
            mean=self.mean(generator),
            stddev=self.stddev(generator),
            rotate_radians=rotate_radians(generator),
        )


@attr.dataclass(frozen=True)
class RandomVector:
    x: RandomNumber
    y: RandomNumber
    z: RandomNumber

    def __call__(self, generator: RandomState) -> Vector:
        return Vector(self.x(generator), self.y(generator), self.z(generator),)
