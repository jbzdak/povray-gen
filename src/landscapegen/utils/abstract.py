import abc
import random
from random import Random
from typing import Optional, Union

import attr
from numpy.random.mtrand import RandomState

from povraygen import POVRayObject

SEED_SIZE = 128

SYSTEM_RANDOM = random.SystemRandom()


def seed_converter(seed: int):
    if seed is None:
        return 42
    return seed


@attr.dataclass(frozen=True)
class PhasedRandomGenerator:
    """
    The idea is to have as much reproducible (random) landscapes as possible,
    however if I'd just have a single generator for the whole process
    each change in the algorithm (eg. getting one more call to random)
    would influence all later stages.
    So each stage gets a new generator seeded by random state from up the
    hierarchy, that way I'll get reproducibility that is more resilient to
    minor changes in the algorithm.
    """

    generator: RandomState
    """
    Source of randomness.
    """

    phase_generator: random.Random
    """
    Source of randomness for generating sub-phases. 
    """

    initial_seed: Optional[Union[int, bytes]] = None

    def create_phase(self):
        return type(self).create(self.phase_generator.getrandbits(SEED_SIZE))

    def new_seed(self) -> int:
        return self.phase_generator.getrandbits(SEED_SIZE)

    @classmethod
    def create_sub_generator(cls, rnd: Random) -> RandomState:
        return RandomState([rnd.getrandbits(32) for __ in range(0, SEED_SIZE, 32)])

    @classmethod
    def from_parent(cls, parent: "Union[PhasedRandomGenerator, RandomState]"):
        if isinstance(parent, PhasedRandomGenerator):
            return cls.create(parent.phase_generator.getrandbits(SEED_SIZE))
        if isinstance(parent, RandomState):
            return cls.create(parent.bytes(SEED_SIZE // 8))
        raise NotImplementedError

    @classmethod
    def create(cls, seed: Union[int, bytes] = None):
        if seed is None:
            seed = SYSTEM_RANDOM.getrandbits(SEED_SIZE)

        phase_generator = random.Random(seed)

        return cls(
            generator=cls.create_sub_generator(phase_generator),
            phase_generator=phase_generator,
            initial_seed=seed,
        )


class PovrayObjectGenerator(abc.ABC):
    @abc.abstractmethod
    def __call__(self, generator: RandomState) -> "POVRayObject":
        raise NotImplementedError
