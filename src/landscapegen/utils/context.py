"""
The idea here is that there are a lot of things that need to have some context injected.
I could spend two days on jurry rigging every single call to pass extra context variable,
or use some thread-local sheningans. Instead you can call ``set_context`` which sets
an interpreter-global variable, then `get_context` reads it.

To have some type safety when calling `get_context` you need to specify what interface
you want to get, and we check if the object satisfies the interface.
"""
import typing

_CONTEXT = None


def set_context(ctx: typing.Any):
    global _CONTEXT
    _CONTEXT = ctx


T = typing.TypeVar("T")


def get_context(type: typing.Type[T]) -> T:
    obj = _CONTEXT
    if isinstance(obj, type):
        return obj
    raise ValueError()
