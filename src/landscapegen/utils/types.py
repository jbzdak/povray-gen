from typing import Union, Tuple

Num = Union[int, float]
CoordBounds = Union[Tuple[Num, Num], Num]
PointRandomBounds = Tuple[CoordBounds, CoordBounds]
XYCoord = Tuple[Num, Num]
