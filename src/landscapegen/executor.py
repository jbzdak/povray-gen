import abc
import os
import sys
import threading
import time
from multiprocessing.pool import Pool, AsyncResult
from subprocess import CalledProcessError
from typing import List, Sequence, Optional

import attr

from landscapegen.generator import LandscapeGenerator, PreparedContext
from landscapegen.generator.abstract import PovrayOptions


class Executor:
    @abc.abstractmethod
    def execute(
        self,
        generator: LandscapeGenerator,
        to_execute: Sequence[int],
        options: PovrayOptions,
    ):
        raise NotImplementedError


@attr.dataclass(frozen=True)
class TimedExecutor(Executor):

    internal: Executor

    def execute(
        self,
        generator: LandscapeGenerator,
        to_execute: Sequence[int],
        options: PovrayOptions,
    ):
        start = time.monotonic()
        self.internal.execute(generator, to_execute, options)
        end = time.monotonic()
        diff = end - start

        print(f"Generation run took {diff}s.")


class SerialExecutor(Executor):
    def execute(
        self,
        genrator: LandscapeGenerator,
        to_execute: Sequence[int],
        options: PovrayOptions,
    ):
        for value in to_execute:
            ctx = genrator.prepare(value)
            try:
                genrator.render_image(ctx, options)
                print(f"Rendered image {options.output_file(ctx.seed)}")
            except CalledProcessError as e:
                print(e.stdout.decode("ascii"))
                raise


@attr.dataclass(frozen=False)
class StatefulParralelExecutor:
    generator: LandscapeGenerator
    to_execute: Sequence[int]

    options: PovrayOptions

    pool_size: int = 8

    max_prepared_contexts: int = 8
    concurrent_renderings: int = 1

    maxtasksperchild: int = 1

    pool: Pool = None  # type: ignore

    lock: threading.RLock = attr.ib(factory=threading.RLock)

    running_preparations: int = 0
    running_generations: int = 0

    prepared_contexts: List[PreparedContext] = attr.ib(factory=list)

    last_exception: Optional[Exception] = None

    def __attrs_post_init__(self):
        self.pool = Pool(
            processes=self.pool,
            maxtasksperchild=self.maxtasksperchild,
            initializer=self.__init_process,
        )

    def __init_process(self):
        os.nice(20)

    def error_callback(self, exception):
        self.last_exception = exception

    def submit_generate(self, ctx: PreparedContext):
        print(f"submit_generate {ctx}")
        with self.lock:
            self.running_generations += 1
            self.pool.apply_async(
                self.generator.render_image,
                args=[ctx, self.options],
                callback=self.generate_success_callback,
                error_callback=self.error_callback,
            )

    def submit_prepare(self, seed: int):
        print(f"submit_prepare {seed}")
        with self.lock:
            self.running_preparations += 1
            self.pool.apply_async(
                self.generator.prepare,
                args=[seed],
                callback=self.prepare_success_callback,
                error_callback=self.error_callback,
            )

    def generations_to_run(self) -> Sequence[PreparedContext]:
        free_workers = max(0, self.concurrent_renderings - self.running_generations)
        self.prepared_contexts.sort()
        to_run = self.prepared_contexts[:free_workers]
        self.prepared_contexts = self.prepared_contexts[free_workers:]
        return to_run

    def preparations_to_run(self):
        files_to_add = (
            self.max_prepared_contexts
            - self.running_preparations
            - len(self.prepared_contexts)
        )
        next_batch = self.to_execute[:files_to_add]
        self.to_execute = self.to_execute[files_to_add:]
        return next_batch

    def do_accounting(self):
        print(f"do_accounting start {self}")
        for context in self.generations_to_run():
            self.submit_generate(context)

        for seed in self.preparations_to_run():
            self.submit_prepare(seed)

        print(f"do_accounting end {self}")

    def prepare_success_callback(self, result: PreparedContext):
        print(f"prepare_success_callback {result}")
        print(f"Will render image image {self.options.output_file(result.seed)}")
        with self.lock:
            self.running_preparations -= 1
            self.prepared_contexts.append(result)
            self.do_accounting()

    def generate_success_callback(self, result):
        print(f"generate_success_callback {result}")
        with self.lock:
            self.running_generations -= 1
            self.do_accounting()

    def is_done(self) -> bool:
        with self.lock:
            if self.running_generations != 0:
                return False
            if self.running_preparations != 0:
                return False
            if len(self.to_execute) > 0:
                return False
            if len(self.prepared_contexts) > 0:
                return False
            return True

    def execute(self):
        self.do_accounting()
        while not self.is_done():
            print(f"Not done {self}")
            time.sleep(1)
            if self.last_exception:
                raise self.last_exception


@attr.dataclass(frozen=False)
class SimpleStatefulParralelExecutor:

    generator: LandscapeGenerator
    to_execute: Sequence[int]

    prepared_contexts: int = 3
    concurrent_renderings: int = 1

    prepare_pool: Pool = None  # type: ignore
    render_pool: Pool = None  # type: ignore

    maxtasksperchild: int = 1

    prepare_results: List[AsyncResult] = attr.ib(factory=list)
    render_results: List[AsyncResult] = attr.ib(factory=list)

    def error_callback(self, exception):
        print(f"Error {exception}")
        sys.exit(1)

    def __attrs_post_init__(self):
        self.prepare_pool = Pool(
            processes=self.prepared_contexts, maxtasksperchild=self.maxtasksperchild
        )
        self.render_pool = Pool(
            processes=self.concurrent_renderings, maxtasksperchild=self.maxtasksperchild
        )

    def generate_success_callback(self, result):
        print("generate_success_callback", result)
        self.render_results.append(
            self.render_pool.apply_async(
                self.generator.render_image,
                [result],
                error_callback=self.error_callback,
            )
        )
        print(self.render_results)

    def execute(self):

        for seed in self.to_execute:
            self.prepare_results.append(
                self.prepare_pool.apply_async(
                    self.generator.prepare,
                    [seed],
                    callback=self.generate_success_callback,
                    error_callback=self.error_callback,
                )
            )

        for result in self.prepare_results:
            result.wait()

        for result in self.render_results:
            result.wait()

        self.prepare_pool.close()
        self.render_pool.close()

    def __del__(self):
        self.prepare_pool.close()
        self.render_pool.close()


@attr.dataclass(frozen=True)
class ParralelExecutor(Executor):
    def execute(
        self,
        generator: LandscapeGenerator,
        to_execute: Sequence[int],
        options: PovrayOptions,
    ):
        stateful = StatefulParralelExecutor(
            generator=generator, to_execute=to_execute, options=options,
        )
        stateful.execute()
