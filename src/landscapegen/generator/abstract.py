import abc
import datetime
import pathlib
from typing import Sequence, Optional

import attr


@attr.dataclass(frozen=True)
class PreparedContext:
    seed: int = attr.ib(order=True)
    working_dir: pathlib.Path
    povray_file: pathlib.Path


@attr.dataclass(frozen=True)
class PovrayOptions:
    # .format compatible pattern with ``seed`` variable
    # where now is representing time when interpreted did initialize
    output_file_pattern: str

    # width and height
    image_size: int = 1000

    # display render progress
    display: bool = False

    output_dir: Optional[pathlib.Path] = None

    quality: int = 9

    def __attrs_post_init__(self):
        if self.output_dir:
            self.output_dir.mkdir(exist_ok=True, parents=True)

    def as_args(self) -> Sequence[str]:
        return [
            f"-w{self.image_size}",
            f"-h{self.image_size}",
            f"-D{1 if self.display else 0}",
            f"-Q{self.quality}",
        ]

    def output_file(self, seed: int) -> pathlib.Path:
        file_name = self.output_file_pattern.format(seed=seed)
        if self.output_dir:
            return self.output_dir / file_name
        return pathlib.Path(file_name)


@attr.dataclass(frozen=True)
class LandscapeGenerator(abc.ABC):

    now: datetime.datetime = attr.ib(kw_only=True, default=datetime.datetime.now())

    clean_up: bool = attr.ib(default=True, kw_only=True)

    @abc.abstractmethod
    def prepare(self, seed: int) -> PreparedContext:
        pass

    @abc.abstractmethod
    def render_image(self, context: PreparedContext, options: PovrayOptions):
        pass
