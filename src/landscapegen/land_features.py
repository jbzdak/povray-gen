import abc
from typing import Iterator, Union, Optional

import attr
from numpy.random.mtrand import RandomState

import povraygen
from landscapegen.color import RandomColor, ConstantColor
from landscapegen.hill_generator import ArrayBackedHeightfield
from landscapegen.misc import NameGeneratorContext
from landscapegen.utils import (
    RandomNumber,
    UniformNumber,
    METER,
)
from landscapegen.utils.abstract import (
    PovrayObjectGenerator,
    PhasedRandomGenerator,
    SEED_SIZE,
)
from landscapegen.utils.context import get_context
from landscapegen.utils.math_utils import PositionGenerator
from povraygen import (
    POVRayObject,
    CompositeObject,
    Cone,
    Pigment,
    Texture,
    Scale,
    Rotate,
    TransformedObject,
    ColorDefinition,
)
from povraygen.dto import DeclareObject


class HeightFieldContext:
    def get_height_field(self) -> ArrayBackedHeightfield:
        raise NotImplementedError


def get_texture(
    pigment: Optional[Union[Pigment, Texture]], color: Optional[ColorDefinition]
) -> Texture:

    texture: Optional[Texture] = None
    if pigment is None:
        pigment = Pigment()

    if texture is None:
        if isinstance(pigment, Texture):
            texture = pigment
            pigment = Pigment()
        else:
            texture = Texture()
    else:
        texture = Texture()

    if color is not None:
        pigment = attr.evolve(pigment, color=color)

    return attr.evolve(texture, pigment=pigment)


@attr.dataclass(kw_only=True)
class AbstractObjectField(PovrayObjectGenerator):

    archetype_count: RandomNumber
    count: RandomNumber
    field: PositionGenerator

    def initialize(self, generator: PhasedRandomGenerator):
        pass

    @abc.abstractmethod
    def generate_archetype(
        self, name_to_set: str, generator: PhasedRandomGenerator
    ) -> DeclareObject:
        raise NotImplementedError

    def __call__(self, generator: RandomState):
        seed = generator.bytes(SEED_SIZE // 8)
        parent = self

        class Result(CompositeObject):
            def __iter__(self) -> Iterator[POVRayObject]:
                yield from parent._generate_field(seed)

        return Result()

    def _generate_field(self, seed: Union[int, bytes]) -> Iterator[POVRayObject]:
        gen = PhasedRandomGenerator.create(seed)

        self.initialize(gen.create_phase())

        name_generator = get_context(NameGeneratorContext).name_generator

        names = []

        archetype_generator = gen.create_phase()

        for ii in range(int(self.archetype_count(gen.generator))):
            name = name_generator()
            names.append(name)
            yield self.generate_archetype(name, archetype_generator)

        height_field = get_context(HeightFieldContext).get_height_field()

        object_generator = gen.create_phase().generator

        field = self.field.make_position(gen.generator)

        for ii in range(int(self.count(gen.generator))):
            position_x, position_z = field(object_generator)
            positions = height_field.get_coordinates_indices(position_x, position_z, 0)
            if positions is None:
                continue

            yield TransformedObject.render_object_with_translation(
                object_generator.choice(names), *positions
            )


@attr.dataclass(kw_only=True)
class GrassFieldSharedProps:

    heights: RandomNumber
    rotation_angle: RandomNumber = UniformNumber((0, 180))
    tilt_angle: RandomNumber = UniformNumber((-12, 12))

    color: Optional[RandomColor] = ConstantColor("Red")

    pigment_archetype: Optional[Union[Pigment, Texture]] = None

    base_radius: RandomNumber = UniformNumber((0.02 * METER, 0.001 * METER))
    cap_radius: RandomNumber = UniformNumber((0.005 * METER, 0.001 * METER))


@attr.dataclass(kw_only=True)
class GrassField(GrassFieldSharedProps, AbstractObjectField):

    texture: Texture = None  # type: ignore

    def initialize(self, generator):
        super().initialize(generator)
        self.texture = get_texture(
            self.pigment_archetype, self.color(generator.generator)
        )

    def generate_archetype(
        self, name_to_set: str, generator: PhasedRandomGenerator
    ) -> DeclareObject:
        gen = generator.generator
        height = self.heights(gen) / 2
        rotation = self.rotation_angle(gen)
        angle_x = self.tilt_angle(gen)
        angle_y = self.tilt_angle(gen)

        cone = Cone(
            base_point=povraygen.Vector(0, -height, 0),
            base_radius=self.base_radius(gen),
            cap_point=povraygen.Vector(0, +height, 0),
            cap_radius=self.cap_radius(gen),
            texture=self.texture,
            no_shadow=True,
            transformations=[
                Scale(0.0001, 1, 1),
                Rotate(0, rotation, 0),
                Rotate(angle_x, 0, angle_y),
            ],
        )

        return DeclareObject(name_to_set, cone)
