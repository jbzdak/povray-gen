import math
import typing
from typing import Iterator, Sequence, Optional

import attr
import numpy as np
from numpy.random.mtrand import RandomState

import povraygen
from landscapegen.color import RandomColor
from landscapegen.hill_generator import HillGenerator, ArrayBackedHeightfield
from landscapegen.utils import (
    UniformNumber,
    RandomNumber,
    ConstantNumber,
    sphere_to_cartesian,
    RandomVector,
)
from landscapegen.utils.abstract import PovrayObjectGenerator
from povraygen import (
    CompositeObject,
    HeightField,
    ColorMapElement,
    SkySphere,
    Pigment,
    Scale,
    Translate,
)
from povraygen import dto, POVRayObject


@attr.dataclass(frozen=True)
class Sun(CompositeObject):
    sun_position: dto.Vector = attr.ib(converter=dto.convert_vector)
    light_color: dto.ColorDefinition = attr.ib(validator=dto.validate_color_definition,)
    ball_color: dto.Pigment = attr.ib(converter=dto.pigment_converter)
    ball_radius: dto.Num

    def __iter__(self) -> Iterator[POVRayObject]:
        yield dto.PointLightSource(
            location=self.sun_position, color=self.light_color,
        )
        yield dto.Sphere(
            location=self.sun_position,
            radius=self.ball_radius,
            pigment=self.ball_color,
            no_shadow=True,
            double_illuminate=True,
        )


@attr.dataclass(frozen=True)
class SunGenerator(PovrayObjectGenerator):

    light_color: RandomColor
    ball_color: RandomColor

    sun_distance: RandomNumber = UniformNumber((100, 1000))
    sun_angle_alpha: RandomNumber = UniformNumber((math.pi / 8, math.pi / 2))
    sun_angle_beta: RandomNumber = UniformNumber((-math.pi, math.pi))
    ball_radius: RandomNumber = ConstantNumber(5)

    def __call__(self, generator: RandomState) -> Sun:
        return Sun(
            sun_position=sphere_to_cartesian(
                self.sun_distance(generator),
                self.sun_angle_alpha(generator),
                self.sun_angle_beta(generator),
            ),
            ball_radius=self.ball_radius(generator),
            light_color="White",
            ball_color="Orange",
        )


T = typing.TypeVar("T")


def with_default(obj: Optional[T], default: T) -> T:
    if obj is not None:
        return obj
    return default


@attr.dataclass(frozen=True)
class RandomHeightField:

    color: RandomColor

    scale: RandomVector

    archetype: Optional[povraygen.HeightField] = None

    def __call__(self, generator: HillGenerator) -> ArrayBackedHeightfield:
        detail_generator = generator.config.random.create_phase()

        scale = self.scale(detail_generator.generator)

        color = self.color(detail_generator.generator)

        archetype = with_default(
            self.archetype, HeightField(height_field="/to/be/initialized")
        )

        pigment = with_default(archetype.pigment, Pigment())

        archetype = attr.evolve(
            archetype,
            pigment=attr.evolve(pigment, color=color),
            transformations=[scale.as_scale()],
        )

        return ArrayBackedHeightfield(
            height_field=generator.create_stage(),
            archetype=archetype,
            # TODO: Sometimes rendered png gets desynced with height field
            # if they are cached separately. So I have disabled caching here.
            # height_field_hash=generator.hash,
        )


@attr.dataclass(frozen=True)
class ColorMapElementGenerator(PovrayObjectGenerator):

    band_width: RandomNumber
    color: RandomColor

    def __call__(self, generator: RandomState) -> ColorMapElement:
        return ColorMapElement(
            location=self.band_width(generator), color=self.color(generator),
        )


@attr.dataclass(frozen=True)
class SkyGenerator(PovrayObjectGenerator):

    gradient: RandomVector
    color_map: Sequence[ColorMapElementGenerator]
    scale: RandomNumber
    translate: RandomNumber

    def __call__(self, generator: RandomState) -> SkySphere:

        gradient = self.gradient(generator)

        scale = self.scale(generator)

        translate = self.translate(generator)

        color_map = [cm(generator) for cm in self.color_map]

        locations = np.zeros(shape=len(color_map))
        for ii, cm in enumerate(color_map):
            base = 0
            if ii > 0:
                base = locations[ii - 1]
            locations[ii] = base + cm.location

        locations /= locations[-1]

        color_map = [
            attr.evolve(cm, location=locations[ii]) for ii, cm in enumerate(color_map)
        ]

        return SkySphere(
            pigment=povraygen.Pigment(
                gradient=gradient,
                color_map=color_map,
                transformations=[Scale(1, scale, 1), Translate(0, translate, 0)],
            )
        )


class NameGenerator:
    """
    Generates names for objects.
    """

    current_id = 0

    def __call__(self) -> str:
        try:
            return "O{:d}".format(self.current_id)
        finally:
            self.current_id += 1


@attr.dataclass
class NameGeneratorContext:
    name_generator: NameGenerator = attr.ib(factory=NameGenerator, kw_only=True)
