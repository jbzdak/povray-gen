import pathlib
import re
import tempfile
from typing import Union, Sequence

import pytest

from povraygen import dto, formatter

pytestmark = pytest.mark.povray


def generate_file(file: dto.POVFile) -> str:
    generator = formatter.PovRaygenGenerator.create(file)
    with tempfile.NamedTemporaryFile() as tmp:
        with open(tmp.name, "w") as f:
            generator.output_sink = f
            generator.render_file()
        with open(tmp.name, "r") as f:
            return f.read()


@pytest.fixture()
def output_file_name():
    file_name = pathlib.Path(tempfile.mktemp(suffix=".png"))
    try:
        yield file_name
    finally:
        file_name.unlink(missing_ok=True)


def assert_file_renders(output_file_name, file: dto.POVFile):
    generator = formatter.PovRaygenGenerator.create(file)
    generator.do_stuff(
        output_file_name, clean_up=True, povray_args=["-w1000", "-h1000", "-D0"]
    )

    assert output_file_name.exists()
    assert output_file_name.stat().st_size > 0


def assert_object_renders(
    output_file_name, obj: Union[dto.POVRayObject, Sequence[dto.POVRayObject]]
):

    contents = []

    if isinstance(obj, Sequence):
        contents.extend(obj)

    contents.extend(
        [
            dto.Camera(
                location=[3, 3, 3], look_at=[0, 0, 0], right=[1, 0, 0], up=[0, 1, 0],
            )
        ]
    )

    file = dto.POVFile(
        preamble="""
#version 3.7;    
#include "colors.inc"
global_settings { ambient_light White }
#default{
    pigment { Red } 
}         
        """,
        contents=contents,
    )

    assert_file_renders(output_file_name, file)


def render_object(obj: dto.POVRayObject):
    return generate_file(dto.POVFile(contents=[obj]))


CLEANUP_RE = re.compile(r"\s+")


def compare_file_with_baseline(expected, actual):
    expected = re.sub(CLEANUP_RE, " ", expected).strip()
    actual = re.sub(CLEANUP_RE, " ", actual).strip()

    assert expected == actual


TESTED_OBJECTS = (
    (
        dto.TransformedObject(
            referenced_object="Ball1",
            transformations=[
                dto.Scale(0.00001, 1, 1),
                dto.Rotate(-30, 0, 0),
                dto.Translate(1, 1, 1),
            ],
        ),
        """
        object { 
            Ball1 
            scale <1e-05, 1.0, 1.0> 
            rotate <-30.0, 0.0, 0.0> 
            translate <1.0, 1.0, 1.0> 
        }""",
    ),
    (
        dto.DeclareObject("Ball1", dto.Sphere(location=[0, 0, 0], radius=1)),
        "#declare Ball1 = sphere { <0.0, 0.0, 0.0> 1 }",
    ),
)


@pytest.mark.parametrize("obj,expected", TESTED_OBJECTS)
def test_transformations(obj, expected):
    actual = render_object(obj)
    compare_file_with_baseline(expected, actual)


SMOKE_TEST_OBJECTS = (
    [
        dto.DeclareObject("Ball1", dto.Sphere(location=[0, 0, 0], radius=1)),
        dto.TransformedObject(
            referenced_object="Ball1",
            transformations=[
                dto.Scale(0.00001, 1, 1),
                dto.Rotate(-30, 0, 0),
                dto.Translate(1, 1, 1),
            ],
        ),
    ],
)


@pytest.mark.parametrize("obj", SMOKE_TEST_OBJECTS)
def test_file_renders(obj, output_file_name):
    assert_object_renders(output_file_name, obj)
