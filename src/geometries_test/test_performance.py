import abc
import pathlib
from datetime import datetime
from typing import Sequence, Mapping, Iterable

import attr

import povraygen
from frozendict import frozendict
from geometries.color_theory import color_palette_attempt_1
from landscapegen.color import (
    RandomPalette,
    ConstantColor,
    ColorContext,
    RandomColorFromContextPalette,
    ConstantPalette,
)
from landscapegen.executor import ParralelExecutor
from landscapegen.generator import LandscapeGenerator, PreparedContext
from landscapegen.generator.abstract import PovrayOptions
from landscapegen.hill_generator import (
    HillGenerator,
    HillGeneratorConfig,
    GenericHillPhase,
    GenerationPhase,
    ArrayBackedHeightfield,
)
from landscapegen.land_features import GrassField, HeightFieldContext
from landscapegen.misc import (
    SunGenerator,
    RandomHeightField,
    Sun,
    SkyGenerator,
    ColorMapElementGenerator,
)
from landscapegen.utils import (
    UniformNumber,
    METER,
    GaussianPositionGenerator,
    UniformPosition,
    RandomVector,
)
from landscapegen.utils.abstract import PhasedRandomGenerator
from landscapegen.utils.context import set_context
from landscapegen.utils.math_utils import ConstantNumber
from povraygen import (
    HeightField,
    SkySphere,
    Camera,
    PovRaygenGenerator,
    TextureFinish,
    Texture,
)


@attr.dataclass()
class BaseStatefulLanscapeGenerator:

    seed: int

    generators: Mapping[str, PhasedRandomGenerator] = attr.ib(
        default=None, kw_only=True
    )

    def __attrs_post_init__(self):

        main_generator = PhasedRandomGenerator.create(self.seed)
        self.generators = frozendict(
            {phase: main_generator.create_phase() for phase in self.generation_phases()}
        )

    def __getitem__(self, phase: str) -> PhasedRandomGenerator:
        return self.generators[phase]

    @classmethod
    @abc.abstractmethod
    def generation_phases(cls):
        raise NotImplementedError


@attr.dataclass(frozen=True)
class LandscapeArchetype:

    colors: RandomPalette

    hillgen_config: HillGeneratorConfig
    hillgen_phases: Sequence[GenerationPhase]

    height_field: RandomHeightField

    suns: Sequence[SunGenerator]

    sky: SkyGenerator

    grass_fields: Sequence[GrassField]

    slow: bool = False


@attr.dataclass()
class StatefulLandscapeGenerator(
    BaseStatefulLanscapeGenerator, ColorContext, HeightFieldContext
):

    archetype: LandscapeArchetype

    landscape: HeightField = None
    suns: Sequence[Sun] = None

    sky: SkySphere = None

    camera: Camera = None

    def get_height_field(self) -> ArrayBackedHeightfield:
        return self.landscape

    @classmethod
    def generation_phases(cls):
        return (
            "colors",
            "sky",
            "suns",
            "hillgen",
            "grass",
        )

    def prepare(self):
        set_context(self)
        self.palette = self.archetype.colors(self["colors"].generator)
        self.prepare_heightfield()
        self.suns = [sun(self["suns"].generator) for sun in self.archetype.suns]
        self.sky = self.archetype.sky(self["sky"].generator)

        position = 300

        camera_position = self.landscape.get_coordinates_indices(
            position, position, 2 * METER
        )

        look_at = [1, camera_position[1], 1]

        self.camera = Camera(location=camera_position, angle=120, look_at=look_at)

    def make_scene(self) -> Iterable[povraygen.POVRayObject]:
        yield from self.make_constant_features()
        yield self.landscape
        yield self.sky
        yield from self.suns
        yield self.camera

        for field in self.archetype.grass_fields:
            field = attr.evolve(field, height_field=self.landscape)
            yield field(self["grass"])

    def render_image(self):
        file = povraygen.POVFile(
            preamble="""   
        // -w320 -h240
        // -w800 -h600 +a0.3
        #version 3.7;    
        #include "colors.inc"
        """,
            contents=self.make_scene(),
        )

        generator = povraygen.PovRaygenGenerator.create(file)

        print(generator.context_dir)

        generator.do_stuff(
            f"/tmp/test-{self.seed}.png",
            clean_up=True,
            povray_args=["-w3000", "-h3000", "-d0"],
        )

    def prepare_pov_file(self):
        file = povraygen.POVFile(
            preamble="""   
               // -w320 -h240
               // -w800 -h600 +a0.3
               #version 3.7;    
               #include "colors.inc"
               """,
            contents=self.make_scene(),
        )
        self.prepare()
        generator = povraygen.PovRaygenGenerator.create(file)
        return generator.render_file()

    def do_stuff(self):
        self.prepare()
        self.render_image()

    def prepare_heightfield(self):

        config = attr.evolve(self.archetype.hillgen_config, random=self["hillgen"],)

        hill_generator = HillGenerator(
            config=config, phases=self.archetype.hillgen_phases
        )

        self.landscape = self.archetype.height_field(hill_generator)

    def make_constant_features(self) -> Iterable[povraygen.POVRayObject]:
        yield povraygen.LiteralEscapeHatch(
            f"#declare RAD = {'on' if self.archetype.slow else 'off'};"
        )
        yield povraygen.LiteralEscapeHatch(
            """
global_settings {
    assumed_gamma 1.5 
    # if(RAD)
    radiosity {
      pretrace_start 0.08
      pretrace_end   0.01
      count 150
      nearest_count 10
      error_bound 0.5
      recursion_limit 3
      low_error_factor 0.5
      gray_threshold 0.0
      minimum_reuse 0.005
      maximum_reuse 0.2
      brightness 1
      adc_bailout 0.005
    }
    # end
}
  """
        )


SLOW = False

ARCHETYPE = LandscapeArchetype(
    colors=ConstantPalette(color_palette_attempt_1()),
    hillgen_config=HillGeneratorConfig(
        random=None, cache=True, stage_resolution=(3000, 3000)
    ),
    hillgen_phases=[
        # Large hill phase large hills that go mostly from east to west
        # that look something, something like post glacial landscape??
        GenericHillPhase(
            hill_count=15,
            mean=((-0.3, 1.3), (-0.1, 1.1)),
            stddev=((0.001, 0.1), (0.6, 3)),
            height=(1, 2),
            introduce_skew=True,
        ),
        # Small hills higher but smaller, and without any direction skew.
        GenericHillPhase(
            hill_count=35,
            mean=((-0.1, 1.1), (-0.1, 1.1)),
            stddev=((0.006, 0.02), (0.006, 0.02)),
            height=(1, 2),
            introduce_skew=True,
        ),
    ],
    height_field=RandomHeightField(
        color=RandomColorFromContextPalette("ground"),
        scale=RandomVector(
            ConstantNumber(1), UniformNumber((0.05, 0.5)), ConstantNumber(1)
        ),
        archetype=HeightField(
            height_field="/placeholder.png", double_illuminate=False, no_shadow=True,
        ),
    ),
    suns=[
        SunGenerator(
            light_color=ConstantColor("White"),
            ball_color=RandomColorFromContextPalette("sun"),
        )
    ],
    sky=SkyGenerator(
        gradient=RandomVector(ConstantNumber(0), ConstantNumber(1), ConstantNumber(0)),
        scale=ConstantNumber(2),
        translate=ConstantNumber(-0.5),
        color_map=[
            ColorMapElementGenerator(
                ConstantNumber(0), RandomColorFromContextPalette("sky")
            ),
            ColorMapElementGenerator(
                UniformNumber((0.01, 0.10)), RandomColorFromContextPalette("sky")
            ),
            ColorMapElementGenerator(
                UniformNumber((0.01, 0.03)), RandomColorFromContextPalette("sky")
            ),
        ],
    ),
    grass_fields=[
        GrassField(
            archetype_count=ConstantNumber(300),
            count=UniformNumber((20000, 40000)),
            heights=UniformNumber((0.1 * METER, 2 * METER)),
            field=GaussianPositionGenerator(
                mean=UniformPosition(((300, 400), (300, 400))),
                stddev=UniformPosition(((50, 100), (50, 100))),
                rotate_radians=True,
            ),
            pigment_archetype=Texture(finish=TextureFinish(ambient="White")),
            color=RandomColorFromContextPalette("grass"),
        )
        for _ in range(4)
    ]
    + [
        GrassField(
            archetype_count=ConstantNumber(300),
            count=UniformNumber((10000, 60000)),
            heights=UniformNumber((0.1 * METER, 2 * METER)),
            field=GaussianPositionGenerator(
                mean=UniformPosition(((300, 750), (300, 750))),
                stddev=UniformPosition(((50, 200), (50, 200))),
                rotate_radians=True,
            ),
            pigment_archetype=Texture(finish=TextureFinish(ambient="White")),
            color=RandomColorFromContextPalette("grass"),
        )
        for _ in range(6)
    ],
    slow=SLOW,
)


NOW = datetime.now()


class DumbGenerator(LandscapeGenerator):
    def prepare(self, seed: int) -> PreparedContext:
        stateful = StatefulLandscapeGenerator(seed=seed, archetype=ARCHETYPE)
        pov_file = stateful.prepare_pov_file()
        return PreparedContext(
            seed=seed, working_dir=pov_file.parent, povray_file=pov_file
        )

    def render_image(self, context: PreparedContext, options: PovrayOptions):

        PovRaygenGenerator.run_povray(
            output=options.output_file(context.seed),
            input=context.povray_file,
            args=options.as_args(),
        )


DEFAULT_OPTIONS = PovrayOptions(
    output_dir=pathlib.Path(f"/tmp/x-test-{NOW.isoformat()}"),
    output_file_pattern="image-{seed}.png",
)

DEFAULT_GENERATOR = DumbGenerator()

if __name__ == "__main__":

    executor = ParralelExecutor()
    executor.execute(DumbGenerator(), list(range(0, 10)), DEFAULT_OPTIONS)
