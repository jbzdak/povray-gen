import pathlib
import shutil
import tempfile

import attr
import pytest

from landscapegen.executor import SerialExecutor, ParralelExecutor
from landscapegen.generator.abstract import PovrayOptions

pytestmark = pytest.mark.povray


@pytest.fixture()
def output_file_name():
    file_name = pathlib.Path(tempfile.mktemp(suffix=".png"))
    try:
        yield file_name
    finally:
        file_name.unlink(missing_ok=True)


from geometries import debug_heightfield, test_generator, generator


@pytest.mark.parametrize(
    "generator, options",
    (
        (debug_heightfield.GENERATOR, debug_heightfield.DEFAULT_OPTIONS),
        (test_generator.GENERATOR, test_generator.DEFAULT_OPTIONS),
    ),
)
def test_file_generator_generator(output_file_name, generator, options: PovrayOptions):

    options = attr.evolve(options, image_size=100)

    executor = SerialExecutor()
    executor.execute(
        generator,
        [None],
        attr.evolve(options, output_file_pattern=str(output_file_name)),
    )

    assert output_file_name.exists()
    assert output_file_name.stat().st_size > 0


@pytest.fixture()
def output_dir():
    file_name = pathlib.Path(tempfile.mkdtemp())
    try:
        yield file_name
    finally:
        shutil.rmtree(file_name)


@pytest.mark.slow
def test_generator(output_dir):
    executor = ParralelExecutor()
    executor.execute(
        generator.DEFAULT_GENERATOR,
        to_execute=[1, 2, 3, 4],
        options=attr.evolve(generator.DEFAULT_OPTIONS, output_dir=output_dir),
    )
    contents = list(output_dir.iterdir())
    assert len(contents) == 4
    for item in contents:
        assert item.suffix == ".png"
        assert item.stat().st_size > 0
