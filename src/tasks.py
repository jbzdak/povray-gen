import os
import pathlib

from invoke import task

SRC = pathlib.Path(__file__).parent

THIRD_PARTY = SRC / "third-party"


def make_pythonpath():
    dirs = [SRC]
    for thing in THIRD_PARTY.iterdir():
        if thing.is_dir():
            dirs.append(thing)

    dirs = map(str, dirs)

    return ":".join(dirs)


PYTHONPATH = make_pythonpath()

ENV = {"PYTHONPATH": PYTHONPATH, "MYPYPATH": PYTHONPATH}


@task
def mypy(ctx):
    with ctx.cd(str(SRC)):
        ctx.run(
            "mypy povraygen landscapegen", env=ENV,
        )


POVRAY_DEFAULT = True

if "CI" in os.environ:
    POVRAY_DEFAULT = False


@task
def run(ctx):
    with ctx.cd(str(SRC)):
        ctx.run(
            "python geometries/generator.py", env=ENV,
        )


@task
def test(ctx, slow=False, povray=POVRAY_DEFAULT):

    selectors = []

    if not slow:
        selectors.append("not slow")
    if not povray:
        selectors.append("not povray")

    selectors_cmd = ""
    if selectors:
        selectors_cmd = "-m '{}'".format(" and ".join((f"({e})" for e in selectors)))
    with ctx.cd(str(SRC)):
        CMD = f"pytest -v {selectors_cmd} ."
        ctx.run(CMD)


@task
def black(ctx):
    ctx.run("black --check . ")


@task
def sync_deps(ctx):
    with ctx.cd(str(SRC)):
        ctx.run("poetry export -f requirements.txt --without-hashes > requirements.txt")
        ctx.run(
            "poetry export -f requirements.txt --without-hashes --dev > requirements-dev.txt"
        )


@task(black, test, mypy)
def all(ctx):
    pass
