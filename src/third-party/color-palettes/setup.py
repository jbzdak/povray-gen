from setuptools import setup

setup(
    name="color_palettes",
    version="1.0.0",
    packages=[
        "color_palettes",
        "color_palettes.seaborn_copy",
        "color_palettes.seaborn_copy.colors",
        "color_palettes.seaborn_copy.external",
        "color_palettes.mpl_copy",
    ],
    package_data={"color_palette": ["data/*"]},
    install_requires=["frozendict"],
    author="jb",
)
