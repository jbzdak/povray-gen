"""Utility functions, mostly for internal use."""
import colorsys
from typing import Tuple

__all__ = ["desaturate", "saturate", "set_hls_values"]


def desaturate(color: Tuple[int, int, int], prop):

    # Check inputs
    if not 0 <= prop <= 1:
        raise ValueError("prop must be between 0 and 1")

    # Convert to hls
    h, l, s = colorsys.rgb_to_hls(*rgb)

    # Desaturate the saturation channel
    s *= prop

    # Convert back to rgb
    new_color = colorsys.hls_to_rgb(h, l, s)

    return new_color


def saturate(color):
    return set_hls_values(color, s=1)


def set_hls_values(color: Tuple[int, int, int], h=None, l=None, s=None):  # noqa

    vals = list(colorsys.rgb_to_hls(*color))
    for i, val in enumerate([h, l, s]):
        if val is not None:
            vals[i] = val

    rgb = colorsys.hls_to_rgb(*vals)
    return rgb
