import colorsys
import typing
from itertools import cycle

import numpy as np

from color_palettes.mpl_copy.colors import (
    colorConverter,
    LinearSegmentedColormap,
    ListedColormap,
    rgb2hex,
)
from .colors import xkcd_rgb, crayons
from .external import husl
from .utils import desaturate, set_hls_values

__all__ = [
    "color_palette",
    "hls_palette",
    "husl_palette",
    "dark_palette",
    "light_palette",
    "diverging_palette",
    "blend_palette",
    "xkcd_palette",
    "crayon_palette",
    "cubehelix_palette",
    "set_color_codes",
    "SeabornColorPalette",
]

from ..utils import Color

SEABORN_PALETTES = dict(
    deep=[
        "#4C72B0",
        "#DD8452",
        "#55A868",
        "#C44E52",
        "#8172B3",
        "#937860",
        "#DA8BC3",
        "#8C8C8C",
        "#CCB974",
        "#64B5CD",
    ],
    deep6=["#4C72B0", "#55A868", "#C44E52", "#8172B3", "#CCB974", "#64B5CD"],
    muted=[
        "#4878D0",
        "#EE854A",
        "#6ACC64",
        "#D65F5F",
        "#956CB4",
        "#8C613C",
        "#DC7EC0",
        "#797979",
        "#D5BB67",
        "#82C6E2",
    ],
    muted6=["#4878D0", "#6ACC64", "#D65F5F", "#956CB4", "#D5BB67", "#82C6E2"],
    pastel=[
        "#A1C9F4",
        "#FFB482",
        "#8DE5A1",
        "#FF9F9B",
        "#D0BBFF",
        "#DEBB9B",
        "#FAB0E4",
        "#CFCFCF",
        "#FFFEA3",
        "#B9F2F0",
    ],
    pastel6=["#A1C9F4", "#8DE5A1", "#FF9F9B", "#D0BBFF", "#FFFEA3", "#B9F2F0"],
    bright=[
        "#023EFF",
        "#FF7C00",
        "#1AC938",
        "#E8000B",
        "#8B2BE2",
        "#9F4800",
        "#F14CC1",
        "#A3A3A3",
        "#FFC400",
        "#00D7FF",
    ],
    bright6=["#023EFF", "#1AC938", "#E8000B", "#8B2BE2", "#FFC400", "#00D7FF"],
    dark=[
        "#001C7F",
        "#B1400D",
        "#12711C",
        "#8C0800",
        "#591E71",
        "#592F0D",
        "#A23582",
        "#3C3C3C",
        "#B8850A",
        "#006374",
    ],
    dark6=["#001C7F", "#12711C", "#8C0800", "#591E71", "#B8850A", "#006374"],
    colorblind=[
        "#0173B2",
        "#DE8F05",
        "#029E73",
        "#D55E00",
        "#CC78BC",
        "#CA9161",
        "#FBAFE4",
        "#949494",
        "#ECE133",
        "#56B4E9",
    ],
    colorblind6=["#0173B2", "#029E73", "#D55E00", "#CC78BC", "#ECE133", "#56B4E9"],
)


MPL_QUAL_PALS = {
    "tab10": 10,
    "tab20": 20,
    "tab20b": 20,
    "tab20c": 20,
    "Set1": 9,
    "Set2": 8,
    "Set3": 12,
    "Accent": 8,
    "Paired": 12,
    "Pastel1": 9,
    "Pastel2": 8,
    "Dark2": 8,
}


QUAL_PALETTE_SIZES = MPL_QUAL_PALS.copy()
QUAL_PALETTE_SIZES.update({k: len(v) for k, v in SEABORN_PALETTES.items()})
QUAL_PALETTES = list(QUAL_PALETTE_SIZES.keys())


class SeabornColorPalette(list):
    """Set the color palette in a with statement, otherwise be a list."""

    def as_colors(self) -> typing.Sequence[Color]:
        return tuple((Color.from_rgb(rgb, 1) for rgb in self))

    def as_hex(self):
        """Return a color palette with hex codes instead of RGB values."""
        hex = [rgb2hex(rgb) for rgb in self]
        return SeabornColorPalette(hex)

    def _repr_html_(self):
        """Rich display of the color palette in an HTML frontend."""
        s = 55
        n = len(self)
        html = f'<svg  width="{n * s}" height="{s}">'
        for i, c in enumerate(self.as_hex()):
            html += (
                f'<rect x="{i * s}" y="0" width="{s}" height="{s}" style="fill:{c};'
                'stroke-width:2;stroke:rgb(255,255,255)"/>'
            )
        html += "</svg>"
        return html


def color_palette(palette=None, n_colors=None, desat=None):
    if not isinstance(palette, str):
        palette = palette
        if n_colors is None:
            n_colors = len(palette)
    else:

        if n_colors is None:
            # Use all colors in a qualitative palette or 6 of another kind
            n_colors = QUAL_PALETTE_SIZES.get(palette, 6)

        if palette in SEABORN_PALETTES:
            # Named "seaborn variant" of old matplotlib default palette
            palette = SEABORN_PALETTES[palette]

        elif palette == "hls":
            # Evenly spaced colors in cylindrical RGB space
            palette = hls_palette(n_colors)

        elif palette == "husl":
            # Evenly spaced colors in cylindrical Lab space
            palette = husl_palette(n_colors)

        elif palette.lower() == "jet":
            # Paternalism
            raise ValueError("No.")

        elif palette.startswith("ch:"):
            # Cubehelix palette with params specified in string
            args, kwargs = _parse_cubehelix_args(palette)
            palette = cubehelix_palette(n_colors, *args, **kwargs)

        else:
            raise ValueError(palette)

    if desat is not None:
        palette = [desaturate(c, desat) for c in palette]

    # Always return as many colors as we asked for
    pal_cycle = cycle(palette)
    palette = [next(pal_cycle) for _ in range(n_colors)]

    # Always return in r, g, b tuple format
    try:
        palette = map(colorConverter.to_rgb, palette)
        palette = SeabornColorPalette(palette)
    except ValueError:
        raise ValueError("Could not generate a palette for %s" % str(palette))

    return palette


def hls_palette(n_colors=6, h=0.01, l=0.6, s=0.65):  # noqa
    hues = np.linspace(0, 1, int(n_colors) + 1)[:-1]
    hues += h
    hues %= 1
    hues -= hues.astype(int)
    palette = [colorsys.hls_to_rgb(h_i, l, s) for h_i in hues]
    return SeabornColorPalette(palette)


def husl_palette(n_colors=6, h=0.01, s=0.9, l=0.65):  # noqa
    hues = np.linspace(0, 1, int(n_colors) + 1)[:-1]
    hues += h
    hues %= 1
    hues *= 359
    s *= 99
    l *= 99  # noqa
    palette = [_color_to_rgb((h_i, s, l), input="husl") for h_i in hues]
    return SeabornColorPalette(palette)


def _color_to_rgb(color, input):
    """Add some more flexibility to color choices."""
    if input == "hls":
        color = colorsys.hls_to_rgb(*color)
    elif input == "husl":
        color = husl.husl_to_rgb(*color)
        color = tuple(np.clip(color, 0, 1))
    elif input == "xkcd":
        color = xkcd_rgb[color]

    return color


def dark_palette(color, n_colors=6, reverse=False, as_cmap=False, input="rgb"):
    color = _color_to_rgb(color, input)
    gray = "#222222"
    colors = [color, gray] if reverse else [gray, color]
    return blend_palette(colors, n_colors, as_cmap)


def light_palette(color, n_colors=6, reverse=False, as_cmap=False, input="rgb"):

    color = _color_to_rgb(color, input)
    light = set_hls_values(color, l=0.95)  # noqa
    colors = [color, light] if reverse else [light, color]
    return blend_palette(colors, n_colors, as_cmap)


def _flat_palette(color, n_colors=6, reverse=False, as_cmap=False, input="rgb"):

    color = _color_to_rgb(color, input)
    flat = desaturate(color, 0)
    colors = [color, flat] if reverse else [flat, color]
    return blend_palette(colors, n_colors, as_cmap)


def diverging_palette(
    h_neg, h_pos, s=75, l=50, sep=10, n=6, center="light", as_cmap=False  # noqa
):

    palfunc = dark_palette if center == "dark" else light_palette
    n_half = int(128 - (sep // 2))
    neg = palfunc((h_neg, s, l), n_half, reverse=True, input="husl")
    pos = palfunc((h_pos, s, l), n_half, input="husl")
    midpoint = dict(light=[(0.95, 0.95, 0.95)], dark=[(0.133, 0.133, 0.133)])[center]
    mid = midpoint * sep
    pal = blend_palette(np.concatenate([neg, mid, pos]), n, as_cmap=as_cmap)
    return pal


def blend_palette(colors, n_colors=6, as_cmap=False, input="rgb"):

    colors = [_color_to_rgb(color, input) for color in colors]
    name = "blend"
    pal = LinearSegmentedColormap.from_list(name, colors)
    if not as_cmap:
        rgb_array = pal(np.linspace(0, 1, int(n_colors)))[:, :3]  # no alpha
        pal = SeabornColorPalette(map(tuple, rgb_array))
    return pal


def xkcd_palette(colors):

    palette = [xkcd_rgb[name] for name in colors]
    return color_palette(palette, len(palette))


def crayon_palette(colors):

    palette = [crayons[name] for name in colors]
    return color_palette(palette, len(palette))


def cubehelix_palette(
    n_colors=6,
    start=0,
    rot=0.4,
    gamma=1.0,
    hue=0.8,
    light=0.85,
    dark=0.15,
    reverse=False,
    as_cmap=False,
):
    def get_color_function(p0, p1):
        # Copied from matplotlib because it lives in private module
        def color(x):
            # Apply gamma factor to emphasise low or high intensity values
            xg = x ** gamma

            # Calculate amplitude and angle of deviation from the black
            # to white diagonal in the plane of constant
            # perceived intensity.
            a = hue * xg * (1 - xg) / 2

            phi = 2 * np.pi * (start / 3 + rot * x)

            return xg + a * (p0 * np.cos(phi) + p1 * np.sin(phi))

        return color

    cdict = {
        "red": get_color_function(-0.14861, 1.78277),
        "green": get_color_function(-0.29227, -0.90649),
        "blue": get_color_function(1.97294, 0.0),
    }

    cmap = LinearSegmentedColormap("cubehelix", cdict)

    x = np.linspace(light, dark, int(n_colors))
    pal = cmap(x)[:, :3].tolist()
    if reverse:
        pal = pal[::-1]

    if as_cmap:
        x_256 = np.linspace(light, dark, 256)
        if reverse:
            x_256 = x_256[::-1]
        pal_256 = cmap(x_256)
        cmap = ListedColormap(pal_256, "seaborn_cubehelix")
        return cmap
    else:
        return SeabornColorPalette(pal)


def _parse_cubehelix_args(argstr):
    """Turn stringified cubehelix params into args/kwargs."""

    if argstr.startswith("ch:"):
        argstr = argstr[3:]

    if argstr.endswith("_r"):
        reverse = True
        argstr = argstr[:-2]
    else:
        reverse = False

    if not argstr:
        return [], {"reverse": reverse}

    all_args = argstr.split(",")

    args = [float(a.strip(" ")) for a in all_args if "=" not in a]

    kwargs = [a.split("=") for a in all_args if "=" in a]
    kwargs = {k.strip(" "): float(v.strip(" ")) for k, v in kwargs}

    kwarg_map = dict(
        s="start", r="rot", g="gamma", h="hue", l="light", d="dark",  # noqa: E741
    )

    kwargs = {kwarg_map.get(k, k): v for k, v in kwargs.items()}

    if reverse:
        kwargs["reverse"] = True

    return args, kwargs


def set_color_codes(palette="deep"):

    if palette == "reset":
        colors = [
            (0.0, 0.0, 1.0),
            (0.0, 0.5, 0.0),
            (1.0, 0.0, 0.0),
            (0.75, 0.0, 0.75),
            (0.75, 0.75, 0.0),
            (0.0, 0.75, 0.75),
            (0.0, 0.0, 0.0),
        ]
    elif not isinstance(palette, str):
        err = "set_color_codes requires a named seaborn palette"
        raise TypeError(err)
    elif palette in SEABORN_PALETTES:
        if not palette.endswith("6"):
            palette = palette + "6"
        colors = SEABORN_PALETTES[palette] + [(0.1, 0.1, 0.1)]
    else:
        err = "Cannot set colors with palette '{}'".format(palette)
        raise ValueError(err)

    for code, color in zip("bgrmyck", colors):
        rgb = colorConverter.to_rgb(color)
        colorConverter.colors[code] = rgb
        colorConverter.cache[code] = rgb
