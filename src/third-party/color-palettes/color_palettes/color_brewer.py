import json
import pathlib
import typing
from collections import defaultdict
from typing import Optional, Iterable

from frozendict import frozendict

DATA = pathlib.Path(__file__).parent / "data" / "cb.json"

import dataclasses

from .utils import Color


ColorPalette = typing.Sequence[Color]


@dataclasses.dataclass(frozen=True)
class ColorSet:
    name: str
    palettes: typing.Mapping[int, ColorPalette]


ColorSets = typing.Mapping[str, ColorSet]


def load_from(file: Optional[Iterable[str]] = None):
    if file is None:
        file = DATA.read_text().split()

    cmyk_colors: dict = json.loads(file[-1])

    return create_color_set(cmyk_colors)


def create_color_set(color_mapping: typing.Mapping):
    color_sets: ColorSets = defaultdict(defaultdict)

    for name, palettes in color_mapping.items():
        converted_palettes = {}
        for count, palettes_cmyk in palettes.items():
            converted_palettes[int(count)] = tuple(
                Color.from_cmyk(color, 100) for color in palettes_cmyk
            )
        color_sets[name] = ColorSet(name, converted_palettes)
    return freeze(color_sets)


def freeze(obj: typing.Union[typing.Sequence, typing.Mapping, typing.Any]):
    if isinstance(obj, typing.Sequence):
        return tuple(map(freeze, obj))
    if isinstance(obj, typing.Mapping):
        return frozendict(obj)
    return obj


COLOR_BREWER = None

try:
    COLOR_BREWER = load_from()
except:
    pass
