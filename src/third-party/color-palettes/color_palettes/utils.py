import dataclasses
from typing import Tuple


@dataclasses.dataclass(frozen=True)
class Color:
    cmyk: Tuple[float, float, float, float]
    rgb: Tuple[float, float, float]

    @classmethod
    def from_cmyk(
        cls, cmyk: Tuple[float, float, float, float], cmyk_scale: float = 100
    ):
        # https://stackoverflow.com/a/47043091/7918
        rgb_scale = 1
        c, m, y, k = map(lambda x: x / cmyk_scale, cmyk)
        r = rgb_scale * (1.0 - c) * (1.0 - k)
        g = rgb_scale * (1.0 - m) * (1.0 - k)
        b = rgb_scale * (1.0 - y) * (1.0 - k)
        return Color(cmyk=cmyk, rgb=(r, g, b))

    @classmethod
    def from_rgb(cls, rgb: Tuple[float, float, float], rgb_scale=255):
        # https://stackoverflow.com/a/14088415/7918
        cmyk_scale = 1
        r, g, b = rgb
        if (r, g, b) == (0, 0, 0):
            # black
            return 0, 0, 0, cmyk_scale

        # rgb [0,255] -> cmy [0,1]
        c = 1 - r / rgb_scale
        m = 1 - g / rgb_scale
        y = 1 - b / rgb_scale

        # extract out k [0, 1]
        min_cmy = min(c, m, y)
        c = (c - min_cmy) / (1 - min_cmy)
        m = (m - min_cmy) / (1 - min_cmy)
        y = (y - min_cmy) / (1 - min_cmy)
        k = min_cmy

        # rescale to the range [0,cmyk_scale]
        return Color(
            rgb=tuple(map(lambda x: x / rgb_scale, rgb)),  # type: ignore
            cmyk=(c * cmyk_scale, m * cmyk_scale, y * cmyk_scale, k * cmyk_scale),
        )
