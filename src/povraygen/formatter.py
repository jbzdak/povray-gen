import collections
import pathlib
import shutil
import subprocess
import tempfile
import typing
from typing import Optional, Type

import attr

from frozendict import frozendict
from landscapegen.generator import PreparedContext
from landscapegen.generator.abstract import PovrayOptions
from povraygen import dto, element_formatters
from povraygen.utils import CACHE_DIR


@attr.s(auto_attribs=True, frozen=True)
class POVRaygenConfig:

    dto_formatter: typing.Mapping[
        typing.Type[dto.PovRayAttribute], element_formatters.ElementFormatter
    ] = attr.ib(default=frozendict(element_formatters.generate_default_formatters()))


# Tests wont be affected by type mismatch
# noinspection PyTypeChecker
def create_doctest_generator():
    return PovRaygenGenerator(
        None,
        None,
        None,
        element_formatters.DoctestSink(),
        element_formatters.generate_default_formatters(),
    )


@attr.dataclass
class PovRaygenGenerator:

    """
    >>> gen = create_doctest_generator()
    >>> gen.render_element(dto.PointLightSource(location=[1000, 10000, -1000], color="White")) # doctest: +NORMALIZE_WHITESPACE
    light_source {
    <1000.0, 10000.0, -1000.0>
    White
    }
    >>> gen.render_element(dto.Sphere([-8, 0, -8], 1, pigment=dto.Pigment("Red"))) # doctest: +NORMALIZE_WHITESPACE
    sphere {
    <-8.0, 0.0, -8.0>
    1
    pigment {
    color Red
    }
    }
    """

    file: dto.POVFile
    context_dir: pathlib.Path
    output_pov_file: pathlib.Path
    output_sink: typing.TextIO

    dto_formatter: typing.MutableMapping[
        typing.Type[dto.PovRayAttribute], element_formatters.ElementFormatter
    ]

    cache: typing.Mapping[
        Type[element_formatters.ElementFormatter], typing.Dict
    ] = collections.defaultdict(dict)

    @classmethod
    def create(
        cls,
        pov_file: dto.POVFile,
        formatter_overrides: typing.Mapping[
            typing.Type[dto.PovRayAttribute], element_formatters.ElementFormatter
        ] = None,
    ):
        formatters = element_formatters.generate_default_formatters()
        if formatter_overrides:
            formatters.update(formatter_overrides)

        context_dir = pathlib.Path(tempfile.mkdtemp(prefix=str(CACHE_DIR / "povray")))

        assert context_dir.exists() and context_dir.is_dir()

        output_pov_file = context_dir / "output.pov"

        return cls(
            file=pov_file,
            context_dir=context_dir,
            output_pov_file=output_pov_file,
            output_sink=output_pov_file.open("x", buffering=8192),
            dto_formatter=formatters,
        )

    @classmethod
    def run_povray(
        cls,
        input: pathlib.Path,
        output: pathlib.Path,
        args: typing.Sequence[str] = tuple(),
    ):
        command = [
            "nice",
            "povray",
            str(input),
            f"-O{str(output.absolute())}",
        ]
        command.extend(args)
        subprocess.check_output(
            command, cwd=str(input.parent), stderr=subprocess.STDOUT
        )

    def find_formatter(
        self, element: dto.PovRayAttribute, name: Optional[str]
    ) -> element_formatters.ElementFormatter:
        element_type = type(element)
        if element_type in self.dto_formatter:
            return self.dto_formatter[element_type]
        for base_class in element_type.mro()[1:]:
            if base_class in self.dto_formatter:
                self.dto_formatter[element_type] = self.dto_formatter[base_class]
                return self.dto_formatter[base_class]
        raise ValueError(
            f"Can't find formatter for {element_type} for {name} ({element})"
        )

    def render_element(self, element: dto.PovRayAttribute, attr: attr.Attribute = None):
        formatter = self.find_formatter(
            element, attr.name if attr is not None else "<unknown>"
        )
        formatter.format(self, element, attr)

    def render_file(self) -> pathlib.Path:
        if self.file.preamble is not None:
            self.output_sink.write(self.file.preamble.preamble_text)
        for element in self.file.contents:
            if isinstance(element, str):
                self.output_sink.write(element)
            if isinstance(element, collections.Iterable):
                for e in element:
                    self.render_element(e)
                continue
            self.render_element(element)
        self.output_sink.flush()
        print(
            f"Output {self.output_pov_file} file size: {self.output_pov_file.stat().st_size / 1024 / 1024}mb"
        )
        return self.output_pov_file

    def close_rendered_file(self):
        self.output_sink.close()

    def run_pov_file(
        self, output_file: pathlib.Path, args: typing.Sequence[str] = tuple()
    ):
        self.output_sink.flush()
        self.run_povray(self.output_pov_file, output_file, args)

    def clean_up(self):
        shutil.rmtree(self.context_dir, ignore_errors=True)

    def do_stuff(
        self,
        output_file: pathlib.Path,
        clean_up=True,
        povray_args: typing.Sequence[str] = tuple(),
    ):
        output_file = pathlib.Path(output_file)
        try:
            self.render_file()
            self.run_pov_file(output_file, povray_args)
            # print(self.output_pov_file.read_text())
        except subprocess.CalledProcessError as e:
            print(e.stdout.decode("ascii"))
            # print(self.output_pov_file.read_text())
            raise
        else:
            if clean_up:
                self.clean_up()

    def prepare(self, seed: int) -> PreparedContext:
        return PreparedContext(
            seed=seed, working_dir=self.context_dir, povray_file=self.render_file()
        )

    def render_image(self, context: PreparedContext, options: PovrayOptions):
        self.run_povray(
            context.povray_file,
            output=options.output_file(context.seed),
            args=options.as_args(),
        )
