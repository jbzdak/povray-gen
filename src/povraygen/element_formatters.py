import enum
import pathlib
import re
import tempfile
import typing
from typing import Optional, Sequence, Union, Generic

import PIL
import PIL.Image
import attr
import numpy as np

from color_palettes.utils import Color
from povraygen import dto, utils
from povraygen import formatter
from povraygen.dto import PovRayAttribute

try:
    from mypy.types import NoneType
except ImportError:
    from builtins import NoneType  # type: ignore


IMG_CACHE = utils.CACHE_DIR / "images"

IMG_CACHE.mkdir(exist_ok=True)


CAPITAL_LETTER = re.compile("[A-Z]")


PovrayType = typing.TypeVar("PovrayType", bound="dto.PovRayAttribute")


class ElementFormatter(typing.Generic[PovrayType]):
    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: PovrayType,
        attribute: typing.Optional[attr.Attribute],
    ):
        raise NotImplementedError


def _print_name(
    context: "formatter.PovRaygenGenerator",
    elem: typing.Any,
    attribute: typing.Optional[attr.Attribute],
    default_name: Optional[str] = None,
):
    output = context.output_sink
    if attribute is not None:
        if not utils.is_field_positional(attribute):
            if utils.is_field_name_from_type(attribute):
                output.write(_StatefulObjectFormatter.get_element_name(elem))
            else:
                output.write(attribute.name)
            output.write(" ")
    else:
        if default_name:
            output.write(default_name)
            output.write(" ")


class SimpleFormatter(ElementFormatter["dto.SimplePOVRayObject"]):
    """
    >>> SimpleFormatter().format(make_doctest_context(),dto.Vector(1, 2, 3),None)
    <1.0, 2.0, 3.0>
    >>> # noinspection PyTypeChecker
    ... SimpleFormatter().format(
    ...    make_doctest_context(),dto.Vector(1, 2, 3), DoctestAttr("scale")
    ... )
    scale <1.0, 2.0, 3.0>
    """

    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: "dto.SimplePOVRayObject",
        attribute: typing.Optional[attr.Attribute],
    ):
        if elem is None:
            return
        output = context.output_sink
        _print_name(context, elem, attribute)
        output.write(elem.render())
        output.write("\n")


class StringFormatter(ElementFormatter[Union[str, pathlib.Path]]):
    def is_quoted(self, attribure):
        return utils.is_string_field_quoted(attribure)

    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: Union[str, pathlib.Path],
        attribute: typing.Optional[attr.Attribute],
    ):
        if elem is None:
            return
        _print_name(context, elem, attribute)
        is_quoted = self.is_quoted(attribute)
        if is_quoted:
            context.output_sink.write('"')
        context.output_sink.write(str(elem))
        if is_quoted:
            context.output_sink.write('" ')


class ColorFormatter(ElementFormatter["Color"]):
    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: "Color",
        attribute: typing.Optional[attr.Attribute],
    ):
        context.output_sink.write(f"rgb<{elem.rgb[0]}, {elem.rgb[1]}, {elem.rgb[2]}>")


class PathFormatter(StringFormatter):
    def is_quoted(self, attribure):
        return True


class NumberFormatter(ElementFormatter["dto.Num"]):
    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: "dto.Num",
        attribute: typing.Optional[attr.Attribute],
    ):
        if elem is None:
            return
        _print_name(context, elem, attribute)
        context.output_sink.write(str(elem))


class BooleanFormatter(ElementFormatter[bool]):
    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: bool,
        attribute: typing.Optional[attr.Attribute],
    ):
        if elem is None:
            return
        flavour = utils.get_boolean_flavour(attribute)
        if flavour == utils.BooleanFlavour.ON_OFF:
            _print_name(context, elem, attribute)
            context.output_sink.write("on" if elem else "off")
        elif flavour == utils.BooleanFlavour.MISSING_PRESENT:
            if elem:
                _print_name(context, elem, attribute)


class EnumFormatter(ElementFormatter[enum.Enum]):
    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: enum.Enum,
        attribute: typing.Optional[attr.Attribute],
    ):
        if elem.value is None:
            return
        context.render_element(elem.value, attribute)


class NoneFormatter(ElementFormatter[NoneType]):
    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: NoneType,
        attribute: typing.Optional[attr.Attribute],
    ):
        pass  # Noop


class SequenceFormatter(ElementFormatter[Sequence[PovRayAttribute]]):  # type: ignore
    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: Sequence[PovRayAttribute],
        attribute: typing.Optional[attr.Attribute],
    ):
        flavour = utils.get_sequence_flavour(attribute)
        if flavour == utils.SequenceFlavour.NAME_THEN_BRACKETS:
            _print_name(context, elem, attribute)
            context.output_sink.write("{")
            for value in elem:
                context.output_sink.write("[")
                context.render_element(value, None)
                context.output_sink.write("]\n")
            context.output_sink.write("}")
        elif flavour == utils.SequenceFlavour.REPEATED_FIELS:
            for value in elem:
                context.render_element(value, attribute)
        else:
            raise ValueError


class CompositeObjectFormatter(ElementFormatter["dto.CompositeObject"]):
    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: "dto.CompositeObject",
        attribute: typing.Optional[attr.Attribute],
    ):
        for e in elem:
            context.render_element(elem, None)


class DeclareObjectFormatter(ElementFormatter["dto.DeclareObject"]):
    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: "dto.DeclareObject",
        attribute: typing.Optional[attr.Attribute],
    ):

        context.output_sink.write(f"#declare {elem.name} = ")
        context.render_element(elem.value)


@attr.s(auto_attribs=True, frozen=True)
class _StatefulObjectFormatter:
    context: "formatter.PovRaygenGenerator"
    elem: "dto.PovRayAttribute"
    attribute: Optional[attr.Attribute]
    output: typing.TextIO

    @classmethod
    def get_element_name(cls, elem: dto.PovRayAttribute) -> str:
        """
        >>> _StatefulObjectFormatter.get_element_name(dto.Pigment())
        'pigment'
        >>> _StatefulObjectFormatter.get_element_name(
        ...     dto.HeightField(height_field="/tmo/foo.png")
        ... )
        'height_field'
        """
        name = type(elem).__name__
        name = name[0].lower() + name[1:]
        name = re.sub(CAPITAL_LETTER, lambda x: "_" + x[0].lower(), name)
        return name

    def format_typical_elements(self, attrs: typing.Sequence[attr.Attribute]):
        for attribute in attrs:
            if not utils.is_ignored(attribute):
                value = getattr(self.elem, attribute.name)
                if value is not None:
                    self.context.render_element(value, attr=attribute)
                    self.context.output_sink.write("\n")

    def format_preamble(self):
        _print_name(
            self.context, self.elem, self.attribute, self.get_element_name(self.elem),
        )

        self.output.write("{\n")

    def format_epilogue(self):
        self.output.write("\n}\n")

    def format(self):
        self.format_preamble()
        self.format_typical_elements(attr.fields(type(self.elem)))
        self.format_epilogue()


class _LightSourceFormatter(_StatefulObjectFormatter):
    @classmethod
    def get_element_name(cls, elem_type: typing.Any) -> str:
        return "light_source"


class _TransformedObjectFormatter(_StatefulObjectFormatter):
    @classmethod
    def get_element_name(cls, elem_type: typing.Any) -> str:
        return "object"


class _LiteralWithProps(_StatefulObjectFormatter):
    @classmethod
    def get_element_name(  # type: ignore[override]
        cls, elem: dto.LiteralWithProps
    ) -> str:
        return elem.name


class _AnonymousFormatter(_StatefulObjectFormatter):
    def format_preamble(self):
        pass

    def format_epilogue(self):
        pass


@attr.s(auto_attribs=True, frozen=True)
class ObjectFormatterMixin(Generic[PovrayType]):
    stateful_formatter: typing.Type[_StatefulObjectFormatter] = _StatefulObjectFormatter

    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: PovrayType,
        attribute: typing.Optional[attr.Attribute],
    ):
        self.stateful_formatter(
            context=context,  # noqa
            elem=elem,  # typing
            attribute=attribute,  # noqa
            output=context.output_sink,  # noqa
        ).format()


class ObjectFormatter(
    ObjectFormatterMixin["dto.POVRayObject"], ElementFormatter["dto.POVRayObject"]
):
    """
    >>> do_format = ObjectFormatter().format
    >>> ctx = make_doctest_context()
    >>> do_format(ctx, dto.Pigment("Red"), None)
    pigment {
    color Red
    }

    >>> # noinspection PyTypeChecker
    ... do_format(ctx, dto.Pigment("Red"), DoctestAttr("not_pigment")) #doctest: +NORMALIZE_WHITESPACE
    not_pigment {
    color Red
    }
    >>> height_field = dto.HeightField("foo.png", smooth=True, pigment="Red")
    >>> do_format(ctx, height_field, None)
    height_field {
    "foo.png"
    smooth
    pigment {
    color Red
    }
    }
    >>> height_field = dto.HeightField(
    ...   "foo.png", smooth=True, pigment="Red", transformations=[dto.Scale(.5, 0, -.5)]
    ... )
    >>> do_format(ctx, height_field, None)
    height_field {
    "foo.png"
    smooth
    pigment {
    color Red
    }
    scale <0.5, 0.0, -0.5>
    }
    """


class HeightFieldFormatter(
    ObjectFormatterMixin["dto.HeightField"], ElementFormatter["dto.HeightField"]
):
    def format(
        self,
        context: "formatter.PovRaygenGenerator",
        elem: "dto.HeightField",
        attribute: typing.Optional[attr.Attribute],
    ):
        if isinstance(elem.height_field, np.ndarray):
            elem = self._replace_array_with_filename(context, elem)
        super().format(context, elem, attribute)

    @classmethod
    def _numpy_array_to_file(
        cls, context: "formatter.PovRaygenGenerator", elem: "dto.HeightField", file_name
    ):
        arr = elem.height_field
        assert isinstance(arr, np.ndarray)
        arr = np.flip(arr, 1)
        array_buffer = arr.T.tobytes()
        img = PIL.Image.new("I", arr.shape)
        img.frombytes(array_buffer, "raw", "I;16")
        img.save(file_name)

    @classmethod
    def _get_rendered_file_name(
        cls, context: "formatter.PovRaygenGenerator", elem: "dto.HeightField"
    ) -> pathlib.Path:
        cache = context.cache[cls]
        if id(elem.height_field) in cache:
            return cache[id(elem.height_field)]
        if elem.height_field_hash is None:
            file_name = pathlib.Path(
                tempfile.mktemp(dir=context.context_dir, suffix=".png")
            )
        else:
            file_name = IMG_CACHE / f"{elem.height_field_hash}.png"
        if not file_name.exists():
            cls._numpy_array_to_file(context, elem, file_name)
        cache[id(elem.height_field)] = file_name
        return file_name

    @classmethod
    def _replace_array_with_filename(
        cls, context: "formatter.PovRaygenGenerator", elem: "dto.HeightField"
    ) -> dto.HeightField:
        file_name = cls._get_rendered_file_name(context, elem)
        return attr.evolve(elem, height_field=file_name)


@attr.s(auto_attribs=True, frozen=True)
class DoctestAttr:
    name: str
    metadata: typing.Optional[typing.Mapping] = None


class DoctestSink:
    @classmethod
    def write(cls, value):
        assert isinstance(value, str), str(value)
        print(value, end="")


# I know types are borked, however tests won't be affected
# noinspection PyTypeChecker
def make_doctest_context() -> "formatter.PovRaygenGenerator":
    return formatter.PovRaygenGenerator(
        file=None,  # type: ignore
        context_dir=None,  # type: ignore
        output_pov_file=None,  # type: ignore
        output_sink=DoctestSink(),  # type: ignore
        dto_formatter=generate_default_formatters(),  # type: ignore
    )


def generate_default_formatters() -> typing.Dict[typing.Type, ElementFormatter]:
    return {
        dto.SimplePOVRayObject: SimpleFormatter(),
        dto.POVRayObject: ObjectFormatter(),
        str: StringFormatter(),
        float: NumberFormatter(),
        int: NumberFormatter(),
        bool: BooleanFormatter(),
        enum.Enum: EnumFormatter(),
        type(None): NoneFormatter(),
        dto.Light: ObjectFormatter(_LightSourceFormatter),
        dto.HeightField: HeightFieldFormatter(),
        pathlib.Path: PathFormatter(),
        list: SequenceFormatter(),
        tuple: SequenceFormatter(),
        dto.AnonymousPovRayObject: ObjectFormatter(_AnonymousFormatter),
        dto.CompositeObject: CompositeObjectFormatter(),
        dto.LiteralWithProps: ObjectFormatter(_LiteralWithProps),
        Color: ColorFormatter(),
        dto.TransformedObject: ObjectFormatter(_TransformedObjectFormatter),
        dto.DeclareObject: DeclareObjectFormatter(),
    }
