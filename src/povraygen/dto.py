import abc
import collections
import enum
import pathlib
import typing
from typing import (
    Optional,
    Union,
    SupportsFloat,
    Sequence,
    Iterable,
    Iterator,
)

import attr
import numpy as np

from color_palettes.utils import Color
from povraygen import utils

Num = Union[int, float]

FloatLike = Union[SupportsFloat, str]

NullableFloatLike = typing.Optional[FloatLike]


try:
    from mypy.types import NoneType
except ImportError:
    from builtins import NoneType  # type: ignore


def nullable_float(value: Optional[FloatLike]) -> typing.Optional[float]:
    if value is None:
        return None
    return float(value)


@attr.s(auto_attribs=True, frozen=True)
class POVRayObject(abc.ABC):
    pass


class AnonymousPovRayObject(POVRayObject):
    pass


PovRaySingleElement = typing.Union[
    POVRayObject, str, int, float, enum.Enum, pathlib.Path, Color, NoneType
]

PovRayAttribute = typing.Union[PovRaySingleElement, Sequence[PovRaySingleElement]]


class SimplePOVRayObject(POVRayObject):
    @abc.abstractmethod
    def render(self):
        raise NotImplementedError


@attr.s(auto_attribs=True, frozen=True)
class Vector(SimplePOVRayObject):
    """
    >>> print(Vector(5).render())
    5.0
    >>> print(Vector(1, 2, 3).render())
    <1.0, 2.0, 3.0>
    """

    x: Num = attr.ib(converter=float)
    y: Optional[Num] = attr.ib(converter=nullable_float, default=None)
    z: Optional[Num] = attr.ib(converter=nullable_float, default=None)

    def render(self):
        if self.y is None and self.z is None:
            return repr(self.x)
        return f"<{self.x}, {self.y}, {self.z}>"

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y if self.y is not None else self.x

    def get_z(self):
        return self.z if self.z is not None else self.x

    def as_scale(self) -> "Scale":
        return Scale(self.x, self.y, self.z)

    @property
    def is_unit_vector(self):
        return (
            self.x == 1
            and (self.y == 1 or self.y is None)
            and (self.z == 1 or self.z is None)
        )


VectorConvertible = Union[Vector, SupportsFloat, Sequence[SupportsFloat]]


class Translate(Vector):
    """
    Moves object by <x, y, z> units from it's current location.
    """

    pass


class Rotate(Vector):
    """
    Rotates object along appropriate axes by <x, y, z>.

    X, Y, Z are in DEGREES.
    """

    pass


class Scale(Vector):
    """
    Moves object by <x, y, z>.
    """

    pass


def convert_vector(value: VectorConvertible) -> Vector:
    if isinstance(value, Vector):
        return value
    if isinstance(value, collections.Sequence):
        assert (
            len(value) == 3
        ), f"Sequence to create vector needs to have 3 elements, and is {value}"
        return Vector(value[0], value[1], value[2])
    return Vector(float(value))


def nullable_convert_vector(value: Optional[VectorConvertible]) -> Optional[Vector]:
    if value is None:
        return None
    return convert_vector(value)


VectorOrInt = typing.Union[Vector, int]


@attr.s(auto_attribs=True, frozen=True)
class RGB(SimplePOVRayObject):
    """
    >>> print(RGB(1.0, 0, 0).render())
    rgb<1.0, 0.0, 0.0>
    """

    r: typing.SupportsFloat = attr.ib(validator=utils.validate_color, converter=float)
    g: typing.SupportsFloat = attr.ib(validator=utils.validate_color, converter=float)
    b: typing.SupportsFloat = attr.ib(validator=utils.validate_color, converter=float)

    def render(self):
        return f"rgb<{self.r}, {self.g}, {self.b}>"


Transformation = Union[Scale, Rotate, Translate]


@attr.s(auto_attribs=True, frozen=True, kw_only=True)
class WithTransformation:
    transformations: Optional[Sequence[Transformation]] = attr.ib(
        default=tuple(),
        metadata=utils.field_metadata(
            sequence_flavour=utils.SequenceFlavour.REPEATED_FIELS,
            field_name_from_type=True,
        ),
        converter=utils.freeze,
        validator=utils.elements_are(Scale, Rotate, Translate),
    )


@attr.s(auto_attribs=True, frozen=True)
class TransformedObjectProps(POVRayObject):
    referenced_object: str = attr.ib(
        metadata=utils.field_metadata(positional_field=True,)
    )


@attr.s(auto_attribs=True, frozen=True)
class TransformedObject(TransformedObjectProps, WithTransformation):
    @classmethod
    def render_object_with_translation(cls, name: str, x: Num, y: Num, z: Num):
        return f"""
object {{
    {name}
    translate <{x}, {y}, {z}>
}}
"""


ColorDefinition = typing.Union[str, RGB, Color]


# Inspection returns false positive --- instance parameter is not used, but we need to conform
# to attr validator API.
def validate_color_definition(instance: object, attrib, value):  # noqa
    if not isinstance(value, (str, RGB, Color)) and value is not None:
        raise ValueError(f"{attrib} must be instance of ColorDefinition and is {value}")
    if isinstance(value, str):
        utils.validate_string_color(value)


@attr.s(auto_attribs=True, frozen=True)
class ColorMapElement(AnonymousPovRayObject):

    location: float = attr.ib(
        validator=utils.validate_in_range(0, 1),
        metadata=utils.field_metadata(positional_field=True),
    )

    color: ColorDefinition = attr.ib(validator=validate_color_definition)


@attr.s(auto_attribs=True, frozen=True)
class PigmentProps:
    color: ColorDefinition = attr.ib(validator=validate_color_definition, default=None)

    gradient: Optional[Vector] = attr.ib(
        converter=nullable_convert_vector, default=None
    )

    transmit: Optional[float] = attr.ib(
        validator=utils.validate_in_range(0, 1), default=None
    )

    color_map: typing.Sequence[ColorMapElement] = attr.ib(
        default=None,
        metadata=utils.field_metadata(
            sequence_flavour=utils.SequenceFlavour.NAME_THEN_BRACKETS
        ),
    )


@attr.s(auto_attribs=True, frozen=True)
class Pigment(POVRayObject, PigmentProps, WithTransformation):

    pass


def pigment_converter(value: Union[Pigment, ColorDefinition]) -> Pigment:
    if isinstance(value, Pigment):
        return value
    return Pigment(color=value)


def nullable_pigment_converter(
    value: Optional[Union[Pigment, ColorDefinition]]
) -> Optional[Pigment]:
    if value is None:
        return None
    return pigment_converter(value)


PigmentConvertible = typing.Union[Pigment, ColorDefinition]


@attr.s(auto_attribs=True, frozen=True)
class TextureFinish(POVRayObject):
    ambient: ColorDefinition = attr.ib(
        validator=validate_color_definition, default=None
    )


@attr.s(auto_attribs=True, frozen=True)
class Texture(POVRayObject):
    finish: Optional[TextureFinish] = None
    pigment: Optional[Pigment] = attr.ib(
        default=None, converter=nullable_pigment_converter
    )


@attr.s(auto_attribs=True, frozen=True, kw_only=True)
class WithModifiers:
    pigment: Optional[Pigment] = attr.ib(
        default=None, converter=nullable_pigment_converter
    )
    texture: Optional[Texture] = attr.ib(default=None)
    no_shadow: Optional[bool] = attr.ib(
        default=None,
        metadata=utils.field_metadata(
            boolean_flavour=utils.BooleanFlavour.MISSING_PRESENT
        ),
    )
    double_illuminate: Optional[bool] = attr.ib(
        default=None,
        metadata=utils.field_metadata(
            boolean_flavour=utils.BooleanFlavour.MISSING_PRESENT
        ),
    )


def validate_heightfield(instance: object, attrib, value: np.ndarray):

    assert isinstance(value, (str, pathlib.Path, np.ndarray))

    if isinstance(value, np.ndarray):
        assert value.dtype == np.dtype(
            "uint16"
        ), "np.array passed to HeightField needs to be uint16 array. Maybe call HeightField.scale_array"


HeightFieldDefinition = typing.Union[str, pathlib.Path, np.ndarray]


@attr.s(auto_attribs=True, frozen=True)
class HeightFieldAttrs:
    height_field: HeightFieldDefinition = attr.ib(
        metadata=utils.field_metadata(positional_field=True, string_field_quoted=True),
        validator=validate_heightfield,
    )

    smooth: bool = attr.ib(
        default=True,
        metadata=utils.field_metadata(
            boolean_flavour=utils.BooleanFlavour.MISSING_PRESENT
        ),
    )

    height_field_hash: typing.Optional[str] = attr.ib(
        metadata=utils.field_metadata(ignore=True), default=None
    )


@attr.s(auto_attribs=True, frozen=True)
class HeightField(POVRayObject, HeightFieldAttrs, WithModifiers, WithTransformation):
    pass


class CameraType(enum.Enum):
    PERSPECTIVE = "perspective"


@attr.s(auto_attribs=True, frozen=True)
class Camera(POVRayObject):
    location: Vector = attr.ib(converter=convert_vector,)
    type: Optional[CameraType] = attr.ib(
        default=None, metadata=utils.field_metadata(positional_field=True)
    )
    right: Optional[Vector] = attr.ib(converter=nullable_convert_vector, default=None)
    up: Optional[Vector] = attr.ib(converter=nullable_convert_vector, default=None)
    angle: Optional[int] = None
    look_at: Optional[Vector] = attr.ib(converter=nullable_convert_vector, default=None)


class Light(POVRayObject):
    pass


@attr.s(auto_attribs=True, frozen=True)
class PointLightSource(Light):

    location: Vector = attr.ib(
        converter=convert_vector, metadata=utils.field_metadata(positional_field=True)
    )
    color: ColorDefinition = attr.ib(
        validator=validate_color_definition,
        metadata=utils.field_metadata(positional_field=True,),
    )


@attr.s(auto_attribs=True, frozen=True)
class SphereProps:
    location: Vector = attr.ib(
        converter=convert_vector, metadata=utils.field_metadata(positional_field=True)
    )
    radius: Num = attr.ib(metadata=utils.field_metadata(positional_field=True))


@attr.s(auto_attribs=True, frozen=True)
class Sphere(POVRayObject, SphereProps, WithModifiers, WithTransformation):
    pass


@attr.s(auto_attribs=True, frozen=True)
class CylinderProps:
    base_point: Vector = attr.ib(
        converter=convert_vector, metadata=utils.field_metadata(positional_field=True)
    )
    cap_point: Vector = attr.ib(
        converter=convert_vector, metadata=utils.field_metadata(positional_field=True)
    )
    radius: Num = attr.ib(metadata=utils.field_metadata(positional_field=True))


@attr.s(auto_attribs=True, frozen=True)
class Cylinder(POVRayObject, CylinderProps, WithModifiers):
    @classmethod
    def horizontal_marker(
        cls, x: Num, z: Num, radius: Num, height=1000, color: ColorDefinition = "Orange"
    ):
        return cls(
            base_point=[x, -height, z],
            cap_point=[x, +height, z],
            radius=radius,
            pigment=Pigment(color=color, transmit=0.6),
        )


@attr.s(auto_attribs=True, frozen=True)
class SkySphereProps:
    pigment: Pigment = attr.ib(default=None, converter=pigment_converter)


@attr.s(auto_attribs=True, frozen=True)
class SkySphere(POVRayObject, SkySphereProps, WithTransformation):
    pass


@attr.s(auto_attribs=True, frozen=True)
class BoxProps:
    corner_1: Vector = attr.ib(
        converter=convert_vector, metadata=utils.field_metadata(positional_field=True)
    )
    corner_2: Vector = attr.ib(
        converter=convert_vector, metadata=utils.field_metadata(positional_field=True)
    )


@attr.s(auto_attribs=True, frozen=True)
class Box(POVRayObject, BoxProps, WithModifiers, WithTransformation):
    pass


@attr.s(auto_attribs=True, frozen=True)
class ConeProps:
    base_point: Vector = attr.ib(
        converter=convert_vector, metadata=utils.field_metadata(positional_field=True)
    )
    base_radius: Num = attr.ib(metadata=utils.field_metadata(positional_field=True))
    cap_point: Vector = attr.ib(
        converter=convert_vector, metadata=utils.field_metadata(positional_field=True)
    )
    cap_radius: Num = attr.ib(metadata=utils.field_metadata(positional_field=True))


@attr.s(auto_attribs=True, frozen=True)
class Cone(POVRayObject, ConeProps, WithModifiers, WithTransformation):
    pass


@attr.s(auto_attribs=True, frozen=True)
class DeclareObject(POVRayObject):
    name: str
    value: POVRayObject


@attr.s(auto_attribs=True, frozen=True)
class Preamble(POVRayObject):
    preamble_text: str = attr.ib(default="", validator=utils.is_not_null)


def preamble_converter(value: Optional[Union[Preamble, str]]) -> Optional[Preamble]:
    if isinstance(value, Preamble):
        return value
    if isinstance(value, str):
        return Preamble(value)
    if value is None:
        return None
    raise ValueError()


@attr.s(auto_attribs=True, frozen=True)
class LiteralProps:
    name: str = attr.ib(
        metadata=utils.field_metadata(string_field_quoted=False, ignore=True)
    )
    literal: str = attr.ib(metadata=utils.field_metadata(positional_field=True))


@attr.s(auto_attribs=True, frozen=True)
class LiteralWithProps(LiteralProps, WithTransformation, WithModifiers):
    pass


@attr.s(auto_attribs=True, frozen=True)
class LiteralEscapeHatch(SimplePOVRayObject):
    literal: str

    def render(self):
        return self.literal


class CompositeObject(POVRayObject):
    @abc.abstractmethod
    def __iter__(self) -> Iterator[POVRayObject]:
        raise NotImplementedError


ObjectGenerator = Iterable[POVRayObject]

SceneContents = Union[ObjectGenerator, POVRayObject, CompositeObject]


@attr.s(auto_attribs=True, frozen=True)
class POVFile:
    contents: typing.Iterable[SceneContents]
    preamble: Optional[Preamble] = attr.ib(converter=preamble_converter, default=None)
