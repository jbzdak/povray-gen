import enum
import pathlib
import tempfile
import typing
from typing import Optional, Union, Sequence, Mapping, Any, TypeVar, Collection

import attr
import numpy as np

from frozendict import frozendict

CACHE_DIR = pathlib.Path(tempfile.gettempdir(), "povraygen-cache")

CACHE_DIR.mkdir(exist_ok=True)

DATA_DIR = pathlib.Path(__file__).parent / "data"


def make_colors():
    with (DATA_DIR / "allowed_colors").open("r") as f:

        colors = set(map(str.strip, f.readlines()))
        colors.discard("")
        colors.discard("\n")
        return colors


COLORS = frozenset(make_colors())


def validate_string_color(color: str):
    """
    :param color:
    :return:
    """
    if color not in COLORS:
        raise ValueError(f"Unknown color passed {color}")


def is_not_null(instance, attribute, value):  # noqa
    if value is None:
        raise ValueError(f"{attribute} can't be None")


def validate_in_range(min, max):
    def validator(instance, attribute, value):  # noqa
        if value is None:
            return
        if 0.0 > value or value > 1.0:
            raise ValueError(
                f"Value needs to be between {min} and {max} (is {value} for {attribute.name})"
            )

    return validator


def elements_are(
    *types: typing.Type,
) -> typing.Callable[[Any, attr.Attribute, Collection], None]:
    def validator(instance, attribute, value: Collection):
        for element in value:
            if not isinstance(element, types):
                raise ValueError(
                    f"Members of {instance} attribute {attribute.name} need to be instances of {types}. Found {element}"
                )

    return validator


def validate_color(instance, attribute, value):  # noqa
    if 0.0 > value or value > 1.0:
        raise ValueError(
            f"RGB coefficient needs to be between [0:1] is {value}, for {attribute}"
        )


def __field_metadata_getter(
    name: str, default_value: typing.Any
) -> typing.Callable[[Optional[attr.Attribute]], typing.Any]:
    def getter(field: Optional[attr.Attribute]):
        metadata = None
        if field is not None:
            metadata = field.metadata
        if metadata is None:
            return default_value
        return metadata.get(name, default_value)

    return getter


class BooleanFlavour(enum.Enum):
    ON_OFF = 1
    """
    will render (for True): 
        foo on 
    or (for False)
        foo off
    """
    MISSING_PRESENT = 2
    """
    For true will render: 
       foo
    for False will not render anything
    """


class SequenceFlavour(enum.Enum):

    NAME_THEN_BRACKETS = 1
    """
    Will render foo { a, b, c} 
    """

    REPEATED_FIELS = 2
    """
    For [Rotation(...), Rotation(...)] 
    will give: 
    rotation  < ... > 
    roration  < ... > 
    """


is_field_positional = __field_metadata_getter("positional_field", False)

is_string_field_quoted = __field_metadata_getter("string_field_quoted", False)

is_field_name_from_type = __field_metadata_getter("field_name_from_type", False)


get_boolean_flavour = __field_metadata_getter(
    "boolean_flavour", BooleanFlavour.MISSING_PRESENT
)

get_sequence_flavour = __field_metadata_getter(
    "sequence_flavour", SequenceFlavour.REPEATED_FIELS
)

is_ignored = __field_metadata_getter("is_ignored", False)


def field_metadata(
    positional_field: bool = False,
    string_field_quoted: bool = False,
    boolean_flavour: BooleanFlavour = None,
    sequence_flavour: SequenceFlavour = None,
    field_name_from_type: bool = False,
    ignore: bool = False,
):
    """

    :param positional_field:
    :param string_field_quoted:
    :param boolean_flavour:
    :param sequence_flavour:
    :param field_name_from_type: If true field name will be always derived from name
                                 of the type passed as valye (e.g. translate, rotate)
                                 instead of property name.
    :param ignore:
    :return:
    """
    metadata_dict: typing.Dict[str, typing.Any] = {}

    if boolean_flavour:
        metadata_dict["boolean_flavour"] = boolean_flavour
    if positional_field:
        metadata_dict["positional_field"] = positional_field
    if string_field_quoted:
        metadata_dict["string_field_quoted"] = string_field_quoted
    if sequence_flavour:
        metadata_dict["sequence_flavour"] = sequence_flavour
    if ignore:
        metadata_dict["is_ignored"] = ignore
    if field_name_from_type:
        metadata_dict["field_name_from_type"] = field_name_from_type
    return metadata_dict


K = TypeVar("K")
V = TypeVar("V")


def freeze_ndarray(obj: np.ndarray) -> np.ndarray:
    obj = np.asarray(obj)
    obj.flags["WRITEABLE"] = False
    return obj


def freeze(obj: Union[Sequence, Mapping, Any]):
    if isinstance(obj, np.ndarray):
        obj.flags["WRITEABLE"] = True
        return obj
    if isinstance(obj, Sequence):
        return tuple(map(freeze, obj))
    if isinstance(obj, Mapping):
        return frozendict(obj)
    return obj


def freeze_map(obj: Mapping[K, V]) -> Mapping[K, V]:
    return freeze(obj)
