# Povray Fun 

This is a hobby project. It generates random landscapes. 

It uses python to generate input files for povray raytracer, and then executes povary to generate a nice image.

The project is in half baked state, as currently most of the time is spend in Python (preparing the files)
and not in povray (doing expensive raytracing), so I decided to rewrite the thing in C++ (repository is pending). 

For now this can generate: 

1. Random ground with random hills; 
2. Sky of randomly generated gradient; 
3. Random "grass"; 

## Examples

Examples are in low resolution, to see them in detail run the code :) 

![Example 1](img/image-1006.png)
![Example 2](img/image-1024.png)
![Example 3](img/image-1036.png)
![Example 4](img/image-2000.png)

## Installation 

### Checkout the repository 

```
git clone https://gitlab.com/jbzdak/povray-gen.git
```

### Download the depenedencies (docker version)

```
docker-compose build 
docker-compose run app inv test
```

### Run the generator (docker version)

```
docker-compose run app inv run
```

This will write some images in the `out` folder in your working copy.

### Download the dependencies (on Linux without docker)

1. Install poetry from https://python-poetry.org/
2. Install povray (`apt-get install povray` or equivalent from your distro)
3. `cd src && poetry install` 

### Run the generator (on Linux without docker)

```
cd src 
poetry shell
export PYTHONPATH=#(pwd)
python geometries/generator.py
```

This will write some images in the `out` folder in your working copy (and possibly a lot of stuff to `/tmp`). 

### Other interesting commands 

* ``inv all`` --- Runs quality checks;


## Documentation 

### How it works

Conceptually this works by: 

1. Generate a valid povray files in Python; 
2. Launch povray ray tracer on the files; 
3. Remove the input files; 

Code is organized as follows: 

* `povraygen` --- classes that generate povray objects (i.e. can be formatted to povray files); 
* `landscapegen` --- classes that generate more complex landscapes from simple povray objects, 

   `executor.py` module here deals with generating povray files and running povray on it; 
* `geometries` --- (mostly) runnable files that glue above two together to generate images;
* `third-party` --- code copied from other repositories, with some wrappers and changes I did; 

### Changing things

Entrypoint to the generation is in `geometries/generator.py`, rest is up to you. 

You can also look at `test_generator.py` and `debug_heightfield.py` (and execute them). 

### Pull requests 

This is a deprecated version of a hobby project, having said that requests (and forks!) are welcome,
however I can't promise that they will be reviewed timely (or all). For Pull requests please keep the CI green (run `inv all` locally to test it). 