FROM python:3.8.5

RUN apt-get update && apt-get -qy install povray curl

COPY build/get-poetry.py /tmp

RUN chmod +x /tmp/get-poetry.py \
    && python /tmp/get-poetry.py --version 1.0.2 \
    && echo 'export PATH="$HOME/.poetry/bin:$PATH"' >> /root/.bashrc

RUN /root/.poetry/bin/poetry config virtualenvs.create false

RUN mkdir /app

WORKDIR /app

COPY src/poetry.lock /app/
COPY src/pyproject.toml /app/
COPY src/third-party/ /app/third-party/

RUN /root/.poetry/bin/poetry install
